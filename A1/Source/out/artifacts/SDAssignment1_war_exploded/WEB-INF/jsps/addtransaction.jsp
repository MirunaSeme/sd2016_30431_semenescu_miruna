<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 04-05-2016
  Time: 1:06 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Make a new transaction</title>
</head>
<body>

<sf:form method="post" action="${pageContext.request.contextPath}/maketransaction" commandName="transaction">
    <table class="formtable">
        <tr>
            <td class="label">Account from which to transfer:</td>
            <td>
                <sf:select class="control" path="idAccount1" name="idAccount1" items="${allClients2}"/><br/>
                <div class="error"><sf:errors path="idAccount1" cssClass="error"></sf:errors></div>
            </td>
        </tr>
        <tr>
            <td class="label">Account to transfer to:</td>
            <td>
                <sf:select class="control" path="idAccount2" name="idAccount2" items="${allClients2}"/><br/>
                <div class="error"><sf:errors path="idAccount2" cssClass="error"></sf:errors></div>
            </td>
        </tr>
        <tr>
            <td class="label">Amount to transfer:</td>
            <td>
                <sf:input class="control" path="amountToTransfer" name="amountToTransfer"/><br/>
                <div class="error"><sf:errors path="amountToTransfer" cssClass="error"></sf:errors></div>
            </td>
        </tr>
        <tr>
            <td class="label"></td>
            <td>
                <input class="control" value="Make Transfer" type="submit"/>
            </td>
        </tr>
    </table>
</sf:form>

</body>
</html>
