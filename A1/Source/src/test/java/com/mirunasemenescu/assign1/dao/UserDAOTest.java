package com.mirunasemenescu.assign1.dao;

import com.mirunasemenescu.assign1.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by Miruna on 03-05-2016.
 */
@ActiveProfiles("dev")
@ContextConfiguration(locations = {"classpath:dao-context.xml", "classpath:security-context.xml",
        "classpath:datasource.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDAOTest {

    @Autowired
    private UserDAO userDAO;
    @Autowired
    private DataSource dataSource;

    @Before
    public void init(){
        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        jdbc.execute("delete from users");
        jdbc.execute("delete from authorities");
    }

    @Test
    public void testCreateUser(){
        User user = new User("mirunica" ,"hellothere", 2940717125790L, true, "ROLE_USER");
        Assert.assertTrue("User creation should return true: ",userDAO.createUser(user));
    }

    @Test
    public void testGetUsers(){
        User user = new User("mirunica" ,"hellothere", 2940717125790L, true, "ROLE_USER");
        userDAO.createUser(user);
        List<User> users = userDAO.getAllUsers();
        Assert.assertEquals("Number of users should be 1", 1, users.size());
    }

    @Test
    public void testExists(){
        User user = new User("mirunica" ,"hellothere", 2940717125790L, true, "ROLE_USER");
        userDAO.createUser(user);
        Assert.assertTrue("User should exist", userDAO.exists(user.getUsername()));
    }

    @Test
    public void testRetrievedUses(){
        User user = new User("mirunica" ,"hellothere", 2940717125790L, true, "ROLE_USER");
        userDAO.createUser(user);
        List<User> users = userDAO.getAllUsers();
        Assert.assertEquals("Created user should be identical to retrieved user", user, users.get(0));
    }
}
