package com.mirunasemenescu.assign1.service;

import com.mirunasemenescu.assign1.dao.ClientDAO;
import com.mirunasemenescu.assign1.dao.UserDAO;
import com.mirunasemenescu.assign1.model.Client;
import com.mirunasemenescu.assign1.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Miruna on 02-05-2016.
 */
@Service("userService")
public class UsersService {

    private UserDAO userDAO;

    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void createUser(User user) {
        userDAO.createUser(user);
    }

    public boolean exists(String username) {
        return userDAO.exists(username);
    }

    @Secured("ROLE_ADMIN")
    public List<User> getAllUsers() {
        return userDAO.getAllUsers();
    }

    public void deleteUser(String username) {
        userDAO.deleteUser(username);
    }

    public void updateUser(User user, String previousUsername) {
        userDAO.updateUser(user, previousUsername);
    }

    public User getUser(String username) {
        return userDAO.getUser(username);
    }
}
