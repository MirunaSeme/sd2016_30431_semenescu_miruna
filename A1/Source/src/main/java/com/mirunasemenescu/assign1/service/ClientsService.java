package com.mirunasemenescu.assign1.service;

import com.mirunasemenescu.assign1.dao.ClientDAO;
import com.mirunasemenescu.assign1.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Miruna on 27-04-2016.
 */
@Service("clientService")
public class ClientsService {

    private ClientDAO clientDAO;

    public List<Client> getCurrent() {
        return clientDAO.getClients();
    }

    @Autowired
    public void setClientDAO(ClientDAO clientDAO) {
        this.clientDAO = clientDAO;
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public void createClient(Client client) {
        clientDAO.createClient(client);
    }

    public void deleteClient(int idClient) {
        clientDAO.deleteClient(idClient);
    }

    public Client getClient(int idClient) {
        return clientDAO.getClient(idClient);
    }

    public void updateClient(Client client) {
        boolean b = clientDAO.updateClient(client);
    }
}
