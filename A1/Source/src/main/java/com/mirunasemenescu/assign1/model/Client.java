package com.mirunasemenescu.assign1.model;

import com.mirunasemenescu.assign1.validation.ValidIdenCard;
import com.mirunasemenescu.assign1.validation.ValidPersNumCode;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by Miruna on 25-04-2016.
 */
public class Client {

    private int idClient;
    @Size(min = 5 ,max=60)
    @Pattern(regexp = ".* .*")
    private String name;
    @NotNull
    @ValidIdenCard(min = 6)
    private int idenCard;
    @ValidPersNumCode(min = 13)
    private long persNumCode;
    @NotEmpty
    private String address;

    public Client(){}

    public Client(String name, int idenCard, long persNumCode, String address) {
        this.address = address;
        this.idenCard = idenCard;
        this.name = name;
        this.persNumCode = persNumCode;
    }

    public Client(int idClient, String name, int idenCard, long persNumCode, String address) {
        this.address = address;
        this.idClient = idClient;
        this.idenCard = idenCard;
        this.name = name;
        this.persNumCode = persNumCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient= idClient;
    }

    public int getIdenCard() {
        return idenCard;
    }

    public void setIdenCard(int idenCard) {
        this.idenCard = idenCard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPersNumCode() {
        return persNumCode;
    }

    public void setPersNumCode(long persNumCode) {
        this.persNumCode = persNumCode;
    }

    @Override
    public String toString() {
        return "Client: " +
                ", ID =" + idClient +
                ", Name ='" + name + '\'' +
                "Address ='" + address + '\'' +
                ", Identity Card Number =" + idenCard +
                ", Personal Numerical Code =" + persNumCode;
    }
}
