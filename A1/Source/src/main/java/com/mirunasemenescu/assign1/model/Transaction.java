package com.mirunasemenescu.assign1.model;

import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Created by Miruna on 04-05-2016.
 */
public class Transaction {
    private int idAccount1;
    private int idAccount2;
    @Min(value = 1)
    @Max(value = 100000)
    private int amountToTransfer;

    public Transaction(){}

    public Transaction(int idAccount1, int idAccount2, int amountToTransfer){
        this.idAccount1 = idAccount1;
        this.idAccount2 = idAccount2;
        this.amountToTransfer = amountToTransfer;
    }

    public int getAmountToTransfer() {
        return amountToTransfer;
    }

    public void setAmountToTransfer(int amountToTransfer) {
        this.amountToTransfer = amountToTransfer;
    }

    public int getIdAccount1() {
        return idAccount1;
    }

    public void setIdAccount1(int idAccount1) {
        this.idAccount1 = idAccount1;
    }

    public int getIdAccount2() {
        return idAccount2;
    }

    public void setIdAccount2(int idAccount2) {
        this.idAccount2 = idAccount2;
    }
}
