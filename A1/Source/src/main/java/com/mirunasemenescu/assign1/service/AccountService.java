package com.mirunasemenescu.assign1.service;

import com.mirunasemenescu.assign1.dao.AccountDAO;
import com.mirunasemenescu.assign1.dao.ClientDAO;
import com.mirunasemenescu.assign1.model.Account;
import com.mirunasemenescu.assign1.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Miruna on 03-05-2016.
 */
@Service("accountService")
public class AccountService {

    private AccountDAO accountDAO;

    public List<Account> getCurrent() {
        return accountDAO.getAccounts();
    }

    public Account getCurrentAccount(int id) {return accountDAO.getAccount(id);}

    public boolean updateAccount(Account account){
        return accountDAO.updateAccount(account);
    }

    @Autowired
    public void setAccountDAO(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public void createAccount(Account account) {
        accountDAO.createAccount(account);
    }

    public void deleteAccout(Integer idAccount) {
        accountDAO.deleteAccount(idAccount);
    }
}
