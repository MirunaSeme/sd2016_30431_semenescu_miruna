package com.mirunasemenescu.assign1.dao;

import com.mirunasemenescu.assign1.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.*;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Miruna on 25-04-2016.
 */
@Component("clientDAO")
public class ClientDAO {

    private NamedParameterJdbcTemplate jdbc;

    // The JDBC template just wraps the data source
    @Autowired
    public void setDataSource(DataSource jdbc) {
        this.jdbc = new NamedParameterJdbcTemplate(jdbc);
    }

    public List<Client> getClients() {
        return jdbc.query("select * from client", new RowMapper<Client>() {
            public Client mapRow(ResultSet resultSet, int i) throws SQLException {
                Client client = new Client();
                client.setIdClient(resultSet.getInt("idClient"));
                client.setName(resultSet.getString("name"));
                client.setIdenCard(resultSet.getInt("idenCard"));
                client.setPersNumCode(resultSet.getLong("persNumCode"));
                client.setAddress(resultSet.getString("address"));
                return client;
            }
        });
    }

    public Client getClient(int idClient) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("idClient", idClient);
        return jdbc.queryForObject("select * from client where idClient = :idClient", params, new RowMapper<Client>() {
                    public Client mapRow (ResultSet resultSet,int i)throws SQLException {
                        Client client = new Client();
                        client.setIdClient(resultSet.getInt("idClient"));
                        client.setName(resultSet.getString("name"));
                        client.setIdenCard(resultSet.getInt("idenCard"));
                        client.setPersNumCode(resultSet.getLong("persNumCode"));
                        client.setAddress(resultSet.getString("address"));
                        return client;
                    }
                }
        );
    }
    public boolean deleteClient(int idClient){
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("idClient", idClient);
        return jdbc.update("delete from client where idClient = :idClient", params) == 1;
    }

    public boolean createClient(Client client){
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(client);
        return jdbc.update("insert into client (name, idenCard, persNumCode, address) values " +
                "(:name, :idenCard, :persNumCode, :address)", params) == 1;
    }

    public boolean updateClient(Client client){
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(client);
        return jdbc.update("update client set name=:name, idenCard=:idenCard, persNumCode=:persNumCode," +
                " address=:address where idClient=:idClient", params) == 1;
    }

    @Transactional
    public int[] createClients(List<Client> clients){
        SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(clients.toArray());
        return jdbc.batchUpdate("insert into client (name, idenCard, persNumCode, address) values " +
                "(:name, :idenCard, :persNumCode, :address)", params);
    }
}
