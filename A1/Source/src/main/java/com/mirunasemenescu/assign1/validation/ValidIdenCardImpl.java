package com.mirunasemenescu.assign1.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by Miruna on 30-04-2016.
 */
public class ValidIdenCardImpl implements ConstraintValidator<ValidIdenCard, Integer>{

    private int min;

    public void initialize(ValidIdenCard constraintAnnotation) {
        min = constraintAnnotation.min();
    }

    public boolean isValid(Integer idenCard, ConstraintValidatorContext context) {
        if(Integer.toString(idenCard).length() == min){
            return true;
        }
        return false;
    }
}
