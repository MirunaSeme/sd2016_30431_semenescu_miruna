package com.mirunasemenescu.assign1.controllers;

import com.mirunasemenescu.assign1.model.Client;
import com.mirunasemenescu.assign1.model.User;
import com.mirunasemenescu.assign1.model.UserList;
import com.mirunasemenescu.assign1.service.UsersService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Miruna on 01-05-2016.
 */

@Controller
public class LoginController {

    private static Logger logger = Logger.getLogger(HomeController.class);
    private UsersService usersService;
    private String previousUsername;

    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    @RequestMapping("/login")
    public String showLogin() {
        return "login";
    }

    @RequestMapping("/logout")
    public String showLogout() {
        return "logout";
    }

    @RequestMapping("/admin")
    public String showAdmin(Model model) {
        List<User> users = usersService.getAllUsers();
        model.addAttribute("users", users);
        return "admin";
    }

    @RequestMapping("/newaccount")
    public String showNewAccount(Model model) {
        model.addAttribute("user", new User());
        return "newaccount";
    }

    @RequestMapping("/delete")
    public String showDelete(Model model) {
        List<String> users = new ArrayList<String>();
        List<User> usersList = usersService.getAllUsers();
        for(User value: usersList) {
            users.add(value.getUsername());
        }
        model.addAttribute("bla", users);
        return "delete";
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public String deleteUser(@RequestParam(value = "id", required = true) String username, Model model, Principal principal) {
        if(username.equals(principal.getName())) {
            model.addAttribute("errorMsg", "Cannot delete yourself!");
            return "admin";
        }
        usersService.deleteUser(username);
        return "userdeleted";
    }

    @RequestMapping(value = "/editUser", method = RequestMethod.GET)
    public String editUser(@RequestParam(value = "id", required = true) String username, Model model) {
        User user = usersService.getUser(username);
        model.addAttribute("user", user);
        this.previousUsername = user.getUsername();
        return "editaccount";
    }

    @RequestMapping("/denied")
    public String showDenied() {
        return "denied";
    }

    @RequestMapping(value = "/editaccount", method = RequestMethod.POST)
    public String editAccount(@Valid User user, BindingResult result, Principal principal) {
        if (result.hasErrors())
            return "editaccount";
        if(user.getUsername().equals(principal.getName()) && user.getAuthority().equals("ROLE_USER")){
            result.rejectValue("authority", "NonUpdateAdmin.user.authority");
            return "editaccount";
        }
        usersService.updateUser(user, previousUsername);
        return "accountupdated";
    }

    @RequestMapping(value = "/createaccount", method = RequestMethod.POST)
    public String createAccount(@Valid User user, BindingResult result) {
        if (result.hasErrors()) {
            return "newaccount";
        }

        //user.setAuthority("ROLE_USER");
        user.setEnabled(true);

        if (usersService.exists(user.getUsername())) {
            result.rejectValue("username", "DuplicateKey.user.username", "This username already exists!");
            return "newaccount";
        }

        try {
            usersService.createUser(user);
        } catch (DuplicateKeyException e) {
            result.rejectValue("username", "DuplicateKey.user.username");
            return "newaccount";
        }

        return "accountcreated";
    }

    @ModelAttribute("allAuthorities")
    public List<String> populateAuthorities(){
        List<String> authorities = new ArrayList<String>();
        authorities.add("ROLE_ADMIN");
        authorities.add("ROLE_USER");
        return authorities;
    }
}