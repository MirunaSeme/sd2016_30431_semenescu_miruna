package com.mirunasemenescu.assign1.filters;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by Miruna on 01-05-2016.
 */
public class DelegatingFilterProxy implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
