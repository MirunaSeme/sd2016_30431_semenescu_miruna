package com.mirunasemenescu.assign1.dao;

import com.mirunasemenescu.assign1.model.Client;
import com.mirunasemenescu.assign1.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Miruna on 25-04-2016.
 */
@Component("userDAO")
public class UserDAO {

    private NamedParameterJdbcTemplate jdbc;

    // The JDBC template just wraps the data source
    @Autowired
    public void setDataSource(DataSource jdbc) {
        this.jdbc = new NamedParameterJdbcTemplate(jdbc);
    }

    @Transactional
    public boolean createUser(User user){

        MapSqlParameterSource params = new MapSqlParameterSource();

        params.addValue("username", user.getUsername());
        params.addValue("password", user.getPassword());
        params.addValue("persNumCode", user.getPersNumCode());
        params.addValue("enabled", user.isEnabled());
        params.addValue("authority", user.getAuthority());

        jdbc.update("insert into users (username, password, enabled, persNumCode) values " +
                "(:username, :password, :enabled, :persNumCode)", params);

        return jdbc.update("insert into authorities (username, authority) values " +
                "(:username, :authority)", params) == 1;
    }

    public boolean exists(String username) {
        return jdbc.queryForObject("select count(*) from users where username=:username",
                new MapSqlParameterSource("username", username),Integer.class) > 0;
    }

    public List<User> getAllUsers() {
        return jdbc.query("select * from users, authorities where users.username=authorities.username",
                BeanPropertyRowMapper.newInstance(User.class));
    }

    public void deleteUser(String username) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("username", username);
        jdbc.update("delete from users where username=:username", params);
        jdbc.update("delete from authorities where username=:username", params);
    }

    @Transactional
    public boolean updateUser(User user, String previousUsername) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("username", user.getUsername());
        params.addValue("authority", user.getAuthority());
        params.addValue("password",user.getPassword());
        params.addValue("enabled", user.isEnabled());
        params.addValue("persNumCode", user.getPersNumCode());
        params.addValue("previousUsername",previousUsername);
        jdbc.update("update authorities set username=:username, authority=:authority where username=:previousUsername", params);
        return jdbc.update("update users set username=:username, password=:password, enabled=:enabled," +
                " persNumCode=:persNumCode where username=:previousUsername", params) == 1;
    }

    public User getUser(String username) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("username", username);
        return jdbc.queryForObject("select users.username, password, enabled, persNumCode, authority " +
                "from users " +
                "inner join authorities on users.username = authorities.username " +
                "where users.username=:username", params, new RowMapper<User>() {
                    public User mapRow (ResultSet resultSet,int i)throws SQLException {
                        User user = new User();
                        user.setUsername(resultSet.getString("username"));
                        user.setPassword(resultSet.getString("password"));
                        user.setPersNumCode(resultSet.getLong("persNumCode"));
                        user.setEnabled(resultSet.getBoolean("enabled"));
                        user.setAuthority(resultSet.getString("authority"));
                        return user;
                    }
                }
        );
    }
}