package com.mirunasemenescu.assign1.model;

import com.mirunasemenescu.assign1.validation.ValidPersNumCode;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by Miruna on 01-05-2016.
 */
public class User {

    @NotBlank
    @Size(min = 4, max = 15)
    @Pattern(regexp = "^\\w{4,}$")
    private String username;
    @NotBlank
    @Pattern(regexp = "^\\S+$")
    @Size(min = 4, max = 15)
    private String password;
    @ValidPersNumCode(min = 13)
    private long persNumCode;
    private boolean enabled = false;
    private String authority;

    public User() {
    }

    public User(String username, String password, long persNumCode, boolean enabled, String authority) {
        this.authority = authority;
        this.enabled = enabled;
        this.password = password;
        this.username = username;
        this.persNumCode = persNumCode;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getPersNumCode() {
        return persNumCode;
    }

    public void setPersNumCode(long persNumCode) {
        this.persNumCode = persNumCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (getPersNumCode() != user.getPersNumCode()) return false;
        if (isEnabled() != user.isEnabled()) return false;
        if (!getUsername().equals(user.getUsername())) return false;
        return getAuthority().equals(user.getAuthority());

    }
}
