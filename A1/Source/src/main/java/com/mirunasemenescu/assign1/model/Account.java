package com.mirunasemenescu.assign1.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * Created by Miruna on 30-04-2016.
 */
public class Account {

    private int idAccount;
    private String accountType;
    @Min(value = 100)
    @Max(value = 1000000000)
    private int amount;
    private String dateOfCreation;
    private String client;

    public Account(){}

    public Account(int idAccount, String accountType, int amount, String dateOfCreation, String client){
        this.idAccount = idAccount;
        this.accountType = accountType;
        this.amount = amount;
        this.dateOfCreation = dateOfCreation;
        this.client = client;
    }

    public Account(String accountType, int amount, String dateOfCreation, String client){
        this.accountType = accountType;
        this.amount = amount;
        this.dateOfCreation = dateOfCreation;
        this.client = client;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(String dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return "Account{" +
                "  idAccount=" + idAccount +
                ", accountType='" + accountType + '\'' +
                ", amount=" + amount +
                ", dateOfCreation='" + dateOfCreation + '\'' +
                ", client='" + client + '\'' +
                '}';
    }
}
