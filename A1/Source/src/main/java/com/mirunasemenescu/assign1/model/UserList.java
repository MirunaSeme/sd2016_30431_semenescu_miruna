package com.mirunasemenescu.assign1.model;

import java.util.List;

/**
 * Created by Miruna on 04-05-2016.
 */
public class UserList {

    private String usernameList;

    public String getUsernameList() {
        return usernameList;
    }

    public void setUsernameList(String usernameList) {
        this.usernameList = usernameList;
    }
}
