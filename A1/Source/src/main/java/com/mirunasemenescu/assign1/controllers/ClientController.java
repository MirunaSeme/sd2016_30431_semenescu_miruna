package com.mirunasemenescu.assign1.controllers;

import com.mirunasemenescu.assign1.model.Client;
import com.mirunasemenescu.assign1.service.ClientsService;
import com.sun.javafx.collections.MappingChange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Created by Miruna on 24-04-2016.
 */

@Controller
public class ClientController {

    private ClientsService clientsService;

    @RequestMapping("/clients")
    public String showClients(Model model){
        List<Client> clients = clientsService.getCurrent();
        model.addAttribute("clients", clients);
        return "clients";
    }

    @RequestMapping(value="/test", method = RequestMethod.GET)
    public String showTest(Model model, @RequestParam("id") String id){
        System.out.println("Id is: " + id);
        return "home";
    }

    @RequestMapping("/addclient")
    public String addClient(Model model){
        model.addAttribute("client", new Client());
        return "addclient";
    }

    @RequestMapping(value="/docreate", method=RequestMethod.POST)
    public String doCreate(Model model, @Valid Client client, BindingResult result){
        if(result.hasErrors()){
            return "addclient";
        }
        clientsService.createClient(client);
        return "clientcreated";
    }

    @Autowired
    public void setClientsService(ClientsService clientsService) {
        this.clientsService = clientsService;
    }

    @RequestMapping(value = "/deleteClient", method = RequestMethod.GET)
    public String deleteUser(@RequestParam(value = "id", required = true) Integer idClient, Model model) {
        clientsService.deleteClient(idClient);
        return "clientdeleted";
    }

    @RequestMapping(value = "/editClient", method = RequestMethod.GET)
    public String editUser(@RequestParam(value = "id", required = true) Integer idClient, Model model) {
        Client client = clientsService.getClient(idClient);
        model.addAttribute("client", client);
        return "editclient";
    }

    @RequestMapping(value = "/editclient", method = RequestMethod.POST)
    public String editAccount(@Valid Client client, BindingResult result) {
        if (result.hasErrors())
            return "editclient";
        clientsService.updateClient(client);
        return "clientupdated";
    }
}
