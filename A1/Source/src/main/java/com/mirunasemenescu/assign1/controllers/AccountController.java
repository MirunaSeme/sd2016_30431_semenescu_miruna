package com.mirunasemenescu.assign1.controllers;

import com.mirunasemenescu.assign1.model.Account;
import com.mirunasemenescu.assign1.model.Client;
import com.mirunasemenescu.assign1.model.Transaction;
import com.mirunasemenescu.assign1.service.AccountService;
import com.mirunasemenescu.assign1.service.ClientsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Miruna on 03-05-2016.
 */
@Controller
public class AccountController {

    private AccountService accountService;
    private ClientsService clientsService;

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Autowired
    public void setClientsService(ClientsService clientsService) {
        this.clientsService = clientsService;
    }

    @RequestMapping("/accounts")
    public String showAccounts(Model model) {
        List<Account> accounts = accountService.getCurrent();
        model.addAttribute("accounts", accounts);
        return "accounts";
    }

    @RequestMapping("/addaccount")
    public String addClient(Model model) {
        model.addAttribute("account", new Account());
        return "addaccount";
    }

    @RequestMapping("/addtransaction")
    public String addTransaction(Model model) {
        model.addAttribute("transaction", new Transaction());
        return "addtransaction";
    }

    @RequestMapping(value = "/maketransaction", method = RequestMethod.POST)
    public String makeTransaction(Model model, @Valid Transaction transaction, BindingResult result) {
        Account account1 = accountService.getCurrentAccount(transaction.getIdAccount1());
        Account account2 = accountService.getCurrentAccount(transaction.getIdAccount2());
        if(account1.getClient().equals(account2.getClient())){
            result.rejectValue("idAccount2", "DuplicateAccount.transaction.idAccount2");
            return "addtransaction";
        }
        int amountTransaction = transaction.getAmountToTransfer();
        if(amountTransaction <= 0){
            //result.rejectValue("amountToTransfer", "NonNegative.transaction.amountToTransfer");
            return "addtransaction";
        }
        int amountAccount1 = account1.getAmount();
        int amountAccount2 = account2.getAmount();
        if(amountAccount1 < amountTransaction){
            result.rejectValue("amountToTransfer", "TooLittle.transaction.amountToTransfer");
            return "addtransaction";
        }
        account1.setAmount(account1.getAmount() - transaction.getAmountToTransfer());
        account2.setAmount(account2.getAmount() + transaction.getAmountToTransfer());
        accountService.updateAccount(account1);
        accountService.updateAccount(account2);
        return "transactionsucc";
    }


    @RequestMapping(value = "/docreateacc", method = RequestMethod.POST)
    public String doCreate(Model model, @Valid Account account, BindingResult result) {
        if (result.hasErrors()) {
            return "addaccount";
        }
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String date = sdf.format(cal.getTime());
        account.setDateOfCreation(date);
        accountService.createAccount(account);
        return "bankaccountcreated";
    }

    @ModelAttribute("allTypes")
    public List<String> populateAccounts() {
        ArrayList<String> accounts = new ArrayList<String>();
        accounts.add("Credit");
        accounts.add("Debit");
        return accounts;
    }

    @ModelAttribute("allClients")
    public Map<String, String> populateClients() {
        Map<String, String> clients = new HashMap<String, String>();
        List<Client> clientsList = clientsService.getCurrent();
        for (Client client : clientsList) {
            clients.put(client.getName(), client.getName());
        }
        return clients;
    }

    @ModelAttribute("allClients2")
    public Map<Integer, String> populateClients2() {
        Map<Integer, String> clients = new HashMap<Integer, String>();
        List<Client> clientsList = clientsService.getCurrent();
        List<Account> accountList = accountService.getCurrent();
        for (Client client : clientsList) {
            for(Account acc: accountList){
                if(acc.getClient().equals(client.getName())){
                    clients.put(acc.getIdAccount(), client.getName() + "  (Account id: " + String.valueOf(acc.getIdAccount()) + ")");
                }
            }
        }
        return clients;
    }

    @RequestMapping(value = "/deleteAccount", method = RequestMethod.GET)
    public String deleteUser(@RequestParam(value = "id", required = true) Integer idAccount, Model model) {
        accountService.deleteAccout(idAccount);
        return "accountdeleted";
    }

    @RequestMapping(value = "/editAccount", method = RequestMethod.GET)
    public String editUser(@RequestParam(value = "id", required = true) Integer idAccount, Model model) {
        Account account = accountService.getCurrentAccount(idAccount);
        model.addAttribute("account", account);
        return "editbankaccount";
    }

    @RequestMapping(value = "/editbankaccount", method = RequestMethod.POST)
    public String editAccount(@Valid Account account, BindingResult result) {
        if (result.hasErrors())
            return "editbankaccount";
        accountService.updateAccount(account);
        return "accountupdated";
    }
}
