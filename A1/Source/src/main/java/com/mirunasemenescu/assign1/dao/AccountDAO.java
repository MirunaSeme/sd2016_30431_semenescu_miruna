package com.mirunasemenescu.assign1.dao;

import com.mirunasemenescu.assign1.model.Account;
import com.mirunasemenescu.assign1.model.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.*;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Miruna on 03-05-2016.
 */
@Component("accountDAO")
public class AccountDAO {

    private NamedParameterJdbcTemplate jdbc;

    // The JDBC template just wraps the data source
    @Autowired
    public void setDataSource(DataSource jdbc) {
        this.jdbc = new NamedParameterJdbcTemplate(jdbc);
    }

    public List<Account> getAccounts() {
        return jdbc.query("select * from accounts", new RowMapper<Account>() {
            public Account mapRow(ResultSet resultSet, int i) throws SQLException {
                Account account = new Account();
                account.setIdAccount(resultSet.getInt("idAccount"));
                account.setAccountType(resultSet.getString("accountType"));
                account.setAmount(resultSet.getInt("amount"));
                account.setDateOfCreation(resultSet.getString("dateOfCreation"));
                account.setClient(resultSet.getString("client"));
                return account;
            }
        });
    }

    public Account getAccount(int idAccount) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("idAccount", idAccount);
        return jdbc.queryForObject("select * from accounts where idAccount = :idAccount", params, new RowMapper<Account>() {
            public Account mapRow (ResultSet resultSet,int i)throws SQLException {
                        Account account = new Account();
                        account.setIdAccount(resultSet.getInt("idAccount"));
                        account.setAccountType(resultSet.getString("accountType"));
                        account.setAmount(resultSet.getInt("amount"));
                        account.setDateOfCreation(resultSet.getString("dateOfCreation"));
                        account.setClient(resultSet.getString("client"));
                        return account;
                    }
                }
        );
    }
    public boolean deleteAccount(int idAccount){
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("idAccount", idAccount);
        return jdbc.update("delete from accounts where idAccount = :idAccount", params) == 1;
    }

    public boolean createAccount(Account account){
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(account);
        return jdbc.update("insert into accounts (accountType, amount, dateOfCreation, client) values " +
                "(:accountType, :amount, :dateOfCreation, :client)", params) == 1;
    }

    public boolean updateAccount(Account account){
        BeanPropertySqlParameterSource params = new BeanPropertySqlParameterSource(account);
        return jdbc.update("update accounts set accountType=:accountType, amount=:amount, dateOfCreation=:dateOfCreation," +
                " client=:client where idAccount=:idAccount", params) == 1;
    }

    @Transactional
    public int[] createAccounts(List<Account> accounts){
        SqlParameterSource[] params = SqlParameterSourceUtils.createBatch(accounts.toArray());
        return jdbc.batchUpdate("insert into accounts (accountType, amount, dateOfCreation, client) values " +
                "(:accountType, :amount, :dateOfCreation, :client)", params);
    }
}
