<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 27-04-2016
  Time: 3:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>

<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Clients</title>
</head>
<body>

<table class="clients">
    <tr><td>Name</td><td>Identity Card Number</td><td>Personal Numerical Code</td><td>Address</td><td>Delete client</td><td>Update client</td></tr>
    <c:forEach var="client" items="${clients}">
        <tr>
            <td><c:out value="${client.name}"/></td>
            <td><c:out value="${client.idenCard}"/></td>
            <td><c:out value="${client.persNumCode}"/></td>
            <td><c:out value="${client.address}"/></td>
            <td><a href=<c:url value="/deleteClient?id=${client.idClient}"/>>Delete</a></td>
            <td><a href=<c:url value="/editClient?id=${client.idClient}"/>>Update</a></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
