<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 03-05-2016
  Time: 9:06 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Accounts</title>
</head>
<body>

<table class="accounts">
    <tr><td>Type of Account</td><td>Amount</td><td>Date of Creation</td><td>Client name</td><td>Delete account</td><td>Update account</td></tr>
    <c:forEach var="account" items="${accounts}">
        <tr>
            <td><c:out value="${account.accountType}"/></td>
            <td><c:out value="${account.amount}"/></td>
            <td><c:out value="${account.dateOfCreation}"/></td>
            <td><c:out value="${account.client}"/></td>
            <td><a href=<c:url value="/deleteAccount?id=${account.idAccount}"/>>Delete</a></td>
            <td><a href=<c:url value="/editAccount?id=${account.idAccount}"/>>Update</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
