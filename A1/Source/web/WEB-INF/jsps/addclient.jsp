<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 27-04-2016
  Time: 3:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Add a new client to the bank</title>
</head>
<body>

<sf:form method="post" action="${pageContext.request.contextPath}/docreate" commandName="client">
    <table class="formtable">
        <tr><td class="label">Name: </td><td>
            <sf:input class="control" path="name" name="name"/><br/>
            <sf:errors path="name" cssClass="error"></sf:errors>
        </td></tr>
        <tr><td class="label">Identity card number: </td><td>
            <sf:input class="control" path="idenCard" name="idenCard"/><br/>
            <sf:errors path="idenCard" cssClass="error"></sf:errors>
        </td></tr>
        <tr><td class="label">Personal numerical code: </td><td>
            <sf:input class="control" path="persNumCode" name="persNumCode"/><br/>
            <sf:errors path="persNumCode" cssClass="error"></sf:errors>
        </td></tr>
        <tr><td class="label">Address: </td><td>
            <sf:input class="control" path="address" name="address"/><br/>
            <sf:errors path="address" cssClass="error"></sf:errors>
        </td></tr>
        <tr><td class="label"> </td><td>
            <input class="control" value="Create client" type="submit"/>
        </td></tr>
    </table>
</sf:form>

</body>
</html>
