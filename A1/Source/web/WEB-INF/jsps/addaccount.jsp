<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 03-05-2016
  Time: 9:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Create a new bank account</title>
</head>
<body>

<sf:form method="post" action="${pageContext.request.contextPath}/docreateacc" commandName="account">
    <table class="formtable">
        <tr>
            <td class="label">Client:</td>
            <td>
                <sf:select class="control" path="client" name="client" items="${allClients}"/><br/>
                <sf:errors path="client" cssClass="error"></sf:errors>
            </td>
        </tr>
        <tr>
            <td class="label">Account Type:</td>
            <td>
                <sf:select class="control" path="accountType" name="accountType" items="${allTypes}"/><br/>
                <sf:errors path="accountType" cssClass="error"></sf:errors>
            </td>
        </tr>
        <tr>
            <td class="label">Amount:</td>
            <td>
                <sf:input class="control" path="amount" name="amount"/><br/>
                <sf:errors path="amount" cssClass="error"></sf:errors>
            </td>
        </tr>
        <tr>
            <td class="label"></td>
            <td>
                <input class="control" value="Create bank account" type="submit"/>
            </td>
        </tr>
    </table>
</sf:form>

</body>
</html>
