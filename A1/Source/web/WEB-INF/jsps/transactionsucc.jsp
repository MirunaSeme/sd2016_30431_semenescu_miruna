<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 04-05-2016
  Time: 1:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Successful Transactions</title>
</head>
<body>

Your transaction has been successful! <a href="${pageContext.request.contextPath}/accounts"> Click here to view current accounts</a>

</body>
</html>
