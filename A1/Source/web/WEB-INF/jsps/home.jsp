<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 21-04-2016
  Time: 9:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Welcome!</title>
</head>
<body>

<div id="header">
    <h1>Welcome!</h1>
</div>

<div id="left">
        <p><a href="${pageContext.request.contextPath}/clients" target="target">Clients</a></p>
    <p><a href="${pageContext.request.contextPath}/accounts" target="target">Accounts</a></p>
        <p><a href="${pageContext.request.contextPath}/addclient" target="target">Register Client</a></p>
        <p><a href="${pageContext.request.contextPath}/addaccount" target="target">Register Bank Account</a></p>
        <p><a href="${pageContext.request.contextPath}/addtransaction" target="target">Make transaction</a></p>
        <sec:authorize access="!isAuthenticated()">
            <p><a target="target" href="<c:url value='/login'/>">Log in </a></p>
        </sec:authorize>
        <sec:authorize access="isAuthenticated()">
            <p><a target="target" href="<c:url value='/j_spring_security_logout'/>">Log out</a></p>
        </sec:authorize>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <p><a target="target" href="<c:url value='/admin'/>">Employees</a></p>
            <p><a target="target" href="<c:url value='/newaccount'/>">New Employee</a></p>
        </sec:authorize>
</div>

<div class="target">
    <iframe name="target" height="500px" width="1000px"></iframe>
</div>


</body>
</html>
