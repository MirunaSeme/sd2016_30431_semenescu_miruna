<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 04-05-2016
  Time: 11:15 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<h2>Remove Employee</h2>
<sf:form method="post" action="${pageContext.request.contextPath}/deleteemployee">
    <table class="formtable">
        <tr>
            <td class="label">Employees: </td>
            <td>
                <sf:select class="control" path="usernameList" name="usernameList" items="${userlist}"/><br/>
                <div class="error"><sf:errors path="usernameList" cssClass="error"></sf:errors></div>
            </td>
        </tr>
        <tr>
            <td class="label"></td>
            <td>
                <input class="control" value="Delete employee" type="submit"/>
            </td>
        </tr>
    </table>
</sf:form>

</body>
</html>
