package com.utilities;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConfigureConnection {
	// Every time we want to connect to the database, we will use the method from this class
	
	public static Connection getConnection(){
		Connection connection = null;
		
		// Connection to database may cause risk such as throwing the application in inconsistent state
		try{
			// Point to the jdbc driver found in the external jar
			Class.forName("com.mysql.jdbc.Driver");
			// Try to retrieve the connection obj
			// arg0 = url to the database
			// arg1 = username of the database
			// arg2 = password of the database
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/university?autoReconnect=true&useSSL=false","root", "1234");
		}catch(Exception e){
			e.printStackTrace();
		}
		return connection;
	}

}
