package com.accessimplm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.access.CourseAccess;
import com.entities.Course;
import com.utilities.ConfigureConnection;

public class CourseAccessImplm implements CourseAccess{

	@Override
	public void createCourseTable() {
		Connection connection = null; 
		Statement statement = null;
		try{
			connection = ConfigureConnection.getConnection();
			statement = connection.createStatement();
			// Table will be created only once (mysql features)
			statement.execute("CREATE TABLE IF NOT EXISTS course (id int primary key unique auto_increment," +
					"name varchar(100), teacher varchar(55), study_year varchar(100))");
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

	@Override
	public void insertCourse(Course course) {
		Connection connection = null;
		// protects us from sql rejection attacks
		PreparedStatement preparedStatement = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			// The ?s protects sql rejection attacks
			preparedStatement = connection.prepareStatement("INSERT INTO course (name, teacher, study_year)" +
					"VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setString(1, course.getName());
			preparedStatement.setString(2, course.getTeacher());
			preparedStatement.setString(3, course.getStudyYear());
			preparedStatement.executeUpdate();
			System.out.println("INSERT INTO course (name, teacher, study_year)" +
					"VALUES (?,?,?)");
			
			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
	            if (generatedKeys.next()) {
	                course.setId(generatedKeys.getInt(1));
	            }
	            else {
	                throw new SQLException("Creating user failed, no ID obtained.");
	            }
	        }
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

	@Override
	public Course selectCourseByID(int id) {
		Course course = new Course();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// It is a table of records from our database. It will hold the results of the query
		ResultSet resultSet = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM course WHERE id=?");
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			
			if(!resultSet.isBeforeFirst()){
				return null;
			}
			
			// Now we have to iterate through resultSet to retrieve values
			while(resultSet.next()){
				course.setId(resultSet.getInt("id"));
				course.setName(resultSet.getString("name"));
				course.setTeacher(resultSet.getString("teacher"));
				course.setStudyYear(resultSet.getString("study_year"));
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(resultSet != null){
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return course;
	}

	@Override
	public List<Course> selectAllCourses() {
		List<Course> courses = new ArrayList<Course>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM course");
			
			while(resultSet.next()){
				Course course = new Course();
				course.setId(resultSet.getInt("id"));
				course.setName(resultSet.getString("name"));
				course.setTeacher(resultSet.getString("teacher"));
				course.setStudyYear(resultSet.getString("study_year"));
				
				courses.add(course);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(resultSet != null){
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return courses;
	}

	@Override
	public void deleteCourseByID(int id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			preparedStatement = connection.prepareStatement("DELETE FROM course WHERE id=?");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			
			System.out.println("DELETE FROM student WHERE id = " +  id);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	@Override
	public void updateCourse(Course course, int id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			preparedStatement = connection.prepareStatement("UPDATE course SET " +
					"name = ?, teacher = ?, study_year = ? WHERE id = ?");
			preparedStatement.setString(1, course.getName());
			preparedStatement.setString(2, course.getTeacher());
			preparedStatement.setString(3, course.getStudyYear());
			preparedStatement.setInt(4, id);
			preparedStatement.executeUpdate();
			
			System.out.println("UPDATE course SET " +
					"name = " + course.getName() + 
					", teacher = ?" + course.getTeacher() + ", teacher = " + course.getStudyYear());
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
		

}
