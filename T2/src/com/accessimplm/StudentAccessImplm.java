package com.accessimplm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.access.StudentAccess;
import com.entities.Student;
import com.utilities.ConfigureConnection;

public class StudentAccessImplm implements StudentAccess{

	public void createStudentTable() {
		Connection connection = null; 
		Statement statement = null;
		try{
			connection = ConfigureConnection.getConnection();
			statement = connection.createStatement();
			// Table will be created only once (mysql features)
			statement.execute("CREATE TABLE IF NOT EXISTS student (id int primary key unique auto_increment," +
					"name varchar(100), birthday varchar(55), address varchar(100))");
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
	}

	public void insertStudent(Student student) {
		Connection connection = null;
		// protects us from sql rejection attacks
		PreparedStatement preparedStatement = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			// The ?s protects sql rejection attacks
			preparedStatement = connection.prepareStatement("INSERT INTO student (name, birthday, address)" +
					"VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setString(1, student.getName());
			preparedStatement.setString(2, student.getBirthday());
			preparedStatement.setString(3, student.getAddress());
			preparedStatement.executeUpdate();
			System.out.println("INSERT INTO student (name, birthday, address)" +
					"VALUES (?,?,?)");
			
			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
	            if (generatedKeys.next()) {
	                student.setId(generatedKeys.getInt(1));
	            }
	            else {
	                throw new SQLException("Creating user failed, no ID obtained.");
	            }
	        }
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public Student selectStudentByID(int id) {
		Student student = new Student();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// It is a table of records from our database. It will hold the results of the query
		ResultSet resultSet = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM student WHERE id=?");
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			
			if(!resultSet.isBeforeFirst()){
				return null;
			}
			
			// Now we have to iterate through resultSet to retrieve values
			while(resultSet.next()){
				student.setId(resultSet.getInt("id"));
				student.setName(resultSet.getString("name"));
				student.setBirthday(resultSet.getString("birthday"));
				student.setAddress(resultSet.getString("address"));
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(resultSet != null){
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return student;
	}

	public List<Student> selectAllStudents() {
		List<Student> students = new ArrayList<Student>();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM student");
			
			while(resultSet.next()){
				Student student = new Student();
				student.setId(resultSet.getInt("id"));
				student.setName(resultSet.getString("name"));
				student.setBirthday(resultSet.getString("birthday"));
				student.setAddress(resultSet.getString("address"));
				
				students.add(student);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(resultSet != null){
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return students;
	}

	public void deleteStudentByID(int id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			preparedStatement = connection.prepareStatement("DELETE FROM student WHERE id=?");
			preparedStatement.setInt(1, id);
			preparedStatement.executeUpdate();
			
			System.out.println("DELETE FROM student WHERE id = " +  id);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void updateStudent(Student student, int id) {
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			preparedStatement = connection.prepareStatement("UPDATE student SET " +
					"name = ?, birthday = ?, address = ? WHERE id = ?");
			preparedStatement.setString(1, student.getName());
			preparedStatement.setString(2, student.getBirthday());
			preparedStatement.setString(3, student.getAddress());
			preparedStatement.setInt(4, id);
			preparedStatement.executeUpdate();
			
			System.out.println("UPDATE student SET " +
					"name = " + student.getName() + 
					", birthday = ?" + student.getBirthday() + ", address = " + student.getAddress());
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	

	

}
