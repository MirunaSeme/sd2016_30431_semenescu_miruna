package com.accessimplm;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.access.EnrollStudentsAccess;
import com.entities.Enrollment;
import com.utilities.ConfigureConnection;

public class EnrollAccessImplm implements EnrollStudentsAccess {

	@Override
	public void createEnrollToCourseTable() {
		Connection connection = null; 
		Statement statement = null;
		try{
			connection = ConfigureConnection.getConnection();
			statement = connection.createStatement();
			// Table will be created only once (mysql features)
			statement.execute("CREATE TABLE IF NOT EXISTS enroll (id int primary key unique auto_increment," +
					"idStudent int, idCourse int)");
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

	@Override
	public void enroll(Enrollment enrollment) {
		Connection connection = null;
		// protects us from sql rejection attacks
		PreparedStatement preparedStatement = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			// The ?s protects sql rejection attacks
			preparedStatement = connection.prepareStatement("INSERT INTO enroll (idStudent, idCourse)" +
					"VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
			
			preparedStatement.setInt(1, enrollment.getStudent().getId());
			preparedStatement.setInt(2, enrollment.getCourse().getId());
			preparedStatement.executeUpdate();
			System.out.println("INSERT INTO enroll (idStudent, idCourse)" +
					"VALUES (?,?)");
			
			try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
	            if (generatedKeys.next()) {
	                enrollment.setId(generatedKeys.getInt(1));
	            }
	            else {
	                throw new SQLException("Creating user failed, no ID obtained.");
	            }
	        }
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if (preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public Enrollment selectEnrollmentByID(int id) {
		Enrollment enrollment = new Enrollment();
		StudentAccessImplm studentAccessImpl = new StudentAccessImplm();
		CourseAccessImplm courseAccessImpl = new CourseAccessImplm();
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		// It is a table of records from our database. It will hold the results of the query
		ResultSet resultSet = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			preparedStatement = connection.prepareStatement("SELECT * FROM enroll WHERE id=?");
			preparedStatement.setInt(1, id);
			resultSet = preparedStatement.executeQuery();
			
			if(!resultSet.isBeforeFirst()){
				return null;
			}
			
			// Now we have to iterate through resultSet to retrieve values
			while(resultSet.next()){
				enrollment.setId(resultSet.getInt("id"));
				enrollment.setStudent(studentAccessImpl.selectStudentByID(resultSet.getInt("idStudent")));
				enrollment.setCourse(courseAccessImpl.selectCourseByID(resultSet.getInt("idCourse")));
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(resultSet != null){
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return enrollment;
	}
	
	public List<Enrollment> selectAllEnrollments(){
		List<Enrollment> enrollments = new ArrayList<Enrollment>();
		StudentAccessImplm studentAccessImpl = new StudentAccessImplm();
		CourseAccessImplm courseAccessImpl = new CourseAccessImplm();
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT * FROM enroll");
			
			while(resultSet.next()){
				Enrollment enrollment = new Enrollment();
				enrollment.setId(resultSet.getInt("id"));
				enrollment.setStudent(studentAccessImpl.selectStudentByID(resultSet.getInt("idStudent")));
				enrollment.setCourse(courseAccessImpl.selectCourseByID(resultSet.getInt("idCourse")));
				
				enrollments.add(enrollment);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(resultSet != null){
				try {
					resultSet.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(statement != null){
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return enrollments;
	}

}
