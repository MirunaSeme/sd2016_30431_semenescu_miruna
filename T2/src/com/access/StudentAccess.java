package com.access;

import java.util.List;

import com.entities.Student;

public interface StudentAccess {
	
	void createStudentTable();
	
	void insertStudent(Student student);
	
	Student selectStudentByID(int id);
	
	List<Student> selectAllStudents();
	
	void deleteStudentByID(int id);
	
	void updateStudent(Student student, int id);
}
