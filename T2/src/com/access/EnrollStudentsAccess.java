package com.access;

import java.util.List;

import com.entities.Enrollment;

public interface EnrollStudentsAccess {
		
		void createEnrollToCourseTable();
		
		void enroll(Enrollment enrollment);
		
		Enrollment selectEnrollmentByID(int id);
		
		List<Enrollment> selectAllEnrollments();
}
