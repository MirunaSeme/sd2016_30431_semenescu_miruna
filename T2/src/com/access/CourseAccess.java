package com.access;

import java.util.List;

import com.entities.Course;

public interface CourseAccess {

	void createCourseTable();
	
	void insertCourse(Course course);
	
	Course selectCourseByID(int id);
	
	List<Course> selectAllCourses();
	
	void deleteCourseByID(int id);
	
	void updateCourse(Course course, int id);
}
