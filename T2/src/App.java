import java.util.List;

import com.accessimplm.CourseAccessImplm;
import com.accessimplm.EnrollAccessImplm;
import com.accessimplm.StudentAccessImplm;
import com.entities.Course;
import com.entities.Enrollment;
import com.entities.Student;


public class App {

	public static void main(String[] args) {
		Student student1 = new Student("Cosmin Popa", "11/06/1985", "Lalelor street, no. 11");
		Student student2 = new Student("Bogdan Marincas" , "13/05/1987", "Clinicilor street, no.32");
		Student student3 = new Student("Denisa Copandean" , "23/11/1999", "Victor Babes street, no. 23");

		Course course1 = new Course("Data Science", "Joldos", "2013, semester 2");
		Course course2 = new Course("Public Speaking", "Calin Muresanu", "2014, semester 1");
		
		Enrollment enrollement1 = new Enrollment(student1, course1);
		Enrollment enrollement2 = new Enrollment(student2, course1);
		Enrollment enrollement3 = new Enrollment(student3, course2);
		
		StudentAccessImplm studentAccessImpl = new StudentAccessImplm();
		CourseAccessImplm courseAccessImpl = new CourseAccessImplm();
		EnrollAccessImplm enrollAccessImplm = new EnrollAccessImplm();
		
		studentAccessImpl.createStudentTable();
		courseAccessImpl.createCourseTable();
		enrollAccessImplm.createEnrollToCourseTable();
		
		studentAccessImpl.insertStudent(student1);
		studentAccessImpl.insertStudent(student2);
		studentAccessImpl.insertStudent(student3);
		
		courseAccessImpl.insertCourse(course1);
		courseAccessImpl.insertCourse(course2);
		
		enrollAccessImplm.enroll(enrollement1);
		enrollAccessImplm.enroll(enrollement2);
		enrollAccessImplm.enroll(enrollement3);

		List<Student> students = studentAccessImpl.selectAllStudents();
		for(Student student : students){
			System.out.println(student.getId() + ", " + student.getName() + ", " + student.getBirthday() + ", " + student.getAddress());
		}
		
		List<Course> courses = courseAccessImpl.selectAllCourses();
		for(Course course : courses){
			System.out.println(course.getId() + ", " + course.getName() + ", " + course.getTeacher() + ", " + course.getStudyYear());
		}
		
		List<Enrollment> enrollments = enrollAccessImplm.selectAllEnrollments();
		for(Enrollment enrollment : enrollments){
			System.out.println(enrollment.getId() + ", Student: " + enrollment.getStudent().getName() + " is enrolled in the course:  " + enrollment.getCourse().getName());
		}

	}

}
