package Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.accessimplm.CourseAccessImplm;
import com.accessimplm.EnrollAccessImplm;
import com.accessimplm.StudentAccessImplm;
import com.entities.Course;
import com.entities.Enrollment;
import com.entities.Student;
import com.utilities.ConfigureConnection;

public class JUnitTesting {

	private StudentAccessImplm studentAccessImpl = new StudentAccessImplm();
	private CourseAccessImplm courseAccessImpl = new CourseAccessImplm();
	private Student student;
	private Course course;
	
	public void insertStudent(){
		this.student = new Student("Maia Bogaia", "03/11/1989", "Brancusi street, no.12");
		this.studentAccessImpl.insertStudent(student);
	}
	
	public void insertCourse(){
		this.course = new Course("Computer Science", "Dinsoreanu", "2007");
		this.courseAccessImpl.insertCourse(course);
	}
	
	@Before
	public void setUp(){
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			preparedStatement = connection.prepareStatement("DELETE FROM student");
			preparedStatement.executeUpdate();
			preparedStatement = connection.prepareStatement("DELETE FROM course");
			preparedStatement.executeUpdate();
			
			System.out.println("DELETE FROM student");
			System.out.println("DELETE FROM course");
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	@After
	public void tearDown(){
		Connection connection = null;
		PreparedStatement preparedStatement = null;
		
		try{
			connection = ConfigureConnection.getConnection();
			preparedStatement = connection.prepareStatement("DELETE FROM student");
			preparedStatement.executeUpdate();
			preparedStatement = connection.prepareStatement("DELETE FROM course");
			preparedStatement.executeUpdate();
			
			System.out.println("DELETE FROM student");
			System.out.println("DELETE FROM course");
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(preparedStatement != null){
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			
			if(connection != null){
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	@Test
	public void addStudent(){
		insertStudent();
		Student addedStudent = studentAccessImpl.selectStudentByID(student.getId());
		assertEquals(student.getName(),addedStudent.getName());
		assertEquals(student.getBirthday(),addedStudent.getBirthday());
		assertEquals(student.getAddress(),addedStudent.getAddress());
	}
	
	@Test
	public void updateStudent(){
		insertStudent();
		String newName = "Mara Bogdana";
		Student studentToUpdated = new Student(newName, "03/11/1989", "Brancusi street, no.12");
		studentAccessImpl.updateStudent(studentToUpdated, student.getId());
		Student updatedStudent = studentAccessImpl.selectStudentByID(student.getId());
		assertEquals(newName, updatedStudent.getName());
	}
	
	@Test
	public void deleteStudent(){
		insertStudent();
		studentAccessImpl.deleteStudentByID(student.getId());
		Student deletedStudent = studentAccessImpl.selectStudentByID(student.getId());
		assertNull(deletedStudent);
		
	}
	
	@Test
	public void addCourse(){
		insertCourse();
		Course addedCourse = courseAccessImpl.selectCourseByID(course.getId());
		assertEquals(course.getName(),addedCourse.getName());
		assertEquals(course.getTeacher(),addedCourse.getTeacher());
		assertEquals(course.getStudyYear(),addedCourse.getStudyYear());
	}
	
	@Test
	public void updateCourse(){
		insertCourse();
		String newName = "Machine Learning";
		Course courseToUpdated = new Course(newName, "Dinsoreanu", "2007");
		courseAccessImpl.updateCourse(courseToUpdated, course.getId());
		Course updatedCourse = courseAccessImpl.selectCourseByID(course.getId());
		assertEquals(newName, updatedCourse.getName());
	}
	
	@Test
	public void deleteCourse(){
		insertCourse();
		courseAccessImpl.deleteCourseByID(course.getId());
		Course deletedCourse = courseAccessImpl.selectCourseByID(course.getId());
		assertNull(deletedCourse);
		
	}
	
	@Test
	public void enrollStudentToCourse(){
		EnrollAccessImplm enrollAccessImplm = new EnrollAccessImplm();
		insertStudent();
		insertCourse();
		Student addedStudent = studentAccessImpl.selectStudentByID(student.getId());
		Course addedCourse = courseAccessImpl.selectCourseByID(course.getId());
		Enrollment enrollment = new Enrollment(addedStudent, addedCourse);
		enrollAccessImplm.enroll(enrollment);
		Enrollment addedEnrollment = enrollAccessImplm.selectEnrollmentByID(enrollment.getId());
		assertEquals(enrollment.getStudent().getName(),addedEnrollment.getStudent().getName());
		assertEquals(enrollment.getStudent().getBirthday(),addedEnrollment.getStudent().getBirthday());
		assertEquals(enrollment.getStudent().getAddress(),addedEnrollment.getStudent().getAddress());
		assertEquals(enrollment.getCourse().getName(),addedEnrollment.getCourse().getName());
		assertEquals(enrollment.getCourse().getTeacher(),addedEnrollment.getCourse().getTeacher());
		assertEquals(enrollment.getCourse().getStudyYear(),addedEnrollment.getCourse().getStudyYear());
	}

}
