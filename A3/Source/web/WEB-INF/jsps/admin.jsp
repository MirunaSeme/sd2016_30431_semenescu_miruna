<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 25-05-2016
  Time: 6:11 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Title</title>
</head>
<body>

<h2>Users</h2>

<p>
    ${errorMsg}
</p>

<table class="admin">
    <tr><td>Username</td><td>Personal Numerical Code</td><td>Role</td><td>Enabled</td><td>Delete user</td><td>Update user</td></tr>
    <c:forEach var="user" items="${users}">
        <tr>
            <td><c:out value="${user.username}"/></td>
            <td><c:out value="${user.persNumCode}"/></td>
            <td><c:out value="${user.authority}"/></td>
            <td><c:out value="${user.enabled}"/></td>
            <td><a href=<c:url value="/deleteUser?id=${user.username}"/>>Delete</a></td>
            <td><a href=<c:url value="/editUser?id=${user.username}"/>>Update</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>

