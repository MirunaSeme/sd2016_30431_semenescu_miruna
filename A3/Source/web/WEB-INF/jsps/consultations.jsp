<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 26-05-2016
  Time: 12:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Title</title>
</head>
<body>

<h2>Consultations</h2>

<p>
    ${errorMsg}
</p>

<table class="admin">
    <tr><td>Date</td><td>Patient Name</td><td>Doctor</td><td>Length Consultation</td><td>Update Consultation</td><td>Delete Consultation</td></tr>
    <c:forEach var="consultation" items="${consultations}">
        <tr>
            <td><c:out value="${consultation.date}"/></td>
            <td><c:out value="${consultation.patient.name}"/></td>
            <td><c:out value="${consultation.user.username}"/></td>
            <td><c:out value="${consultation.lengthOfConsultation}"/></td>
            <td><c:out value="${consultation.notes}"/></td>
            <td><a href=<c:url value="/editConsultation?id=${consultation.id}"/>>Update</a></td>
            <td><a href=<c:url value="/deleteConsultation?id=${consultation.id}"/>>Delete</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
