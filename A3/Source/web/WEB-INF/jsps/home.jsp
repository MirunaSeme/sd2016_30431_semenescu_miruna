<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 22-05-2016
  Time: 8:01 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Welcome!</title>
</head>
<body>

<div id="header">
    <h1>Welcome!</h1>
</div>

<div id="left">
    <p><a href="${pageContext.request.contextPath}/adduser" target="target">Add User</a></p>
    <sec:authorize access="!isAuthenticated()">
        <p><a target="target" href="<c:url value='/login'/>">Log in </a></p>
    </sec:authorize>
    <sec:authorize access="isAuthenticated()">
        <p><a href="${pageContext.request.contextPath}/consultations" target="target">Consultations</a></p>
        <p><a href="${pageContext.request.contextPath}/patients" target="target">Patients</a></p>
        <p><a target="target" href="<c:url value='/j_spring_security_logout'/>">Log out</a></p>
        <p><a href="${pageContext.request.contextPath}/addpatient" target="target">Add Patient</a></p>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <p><a target="target" href="<c:url value='/admin'/>">Users</a></p>
    </sec:authorize>
</div>

<div class="target">
    <iframe name="target" height="500px" width="1000px"></iframe>
</div>


</body>
</html>
