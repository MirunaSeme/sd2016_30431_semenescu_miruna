<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 25-05-2016
  Time: 6:28 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Title</title>
</head>
<body>

<h2>Patients</h2>

<p>
    ${errorMsg}
</p>

<table class="admin">
    <tr><td>Name</td><td>Identity Card</td><td>Personal Numerical Code</td><td>Address</td><td>Date Of Birth</td><td>Update Patient</td><td>Add Consultation</td></tr>
    <c:forEach var="patient" items="${patients}">
        <tr>
            <td><c:out value="${patient.name}"/></td>
            <td><c:out value="${patient.idenCard}"/></td>
            <td><c:out value="${patient.persNumCode}"/></td>
            <td><c:out value="${patient.address}"/></td>
            <td><c:out value="${patient.dateOfBirth}"/></td>
            <td><a href=<c:url value="/editPatient?id=${patient.id}"/>>Update</a></td>
            <td><a href=<c:url value="/addConsult?id=${patient.id}"/>>Add Consultation</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
