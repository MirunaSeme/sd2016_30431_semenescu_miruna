<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 26-05-2016
  Time: 12:18 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Add a new consultation</title>
</head>
<body>

<sf:form method="post" action="${pageContext.request.contextPath}/addconsultation" commandName="consultation">
    <table class="formtable">

        <sf:input type="hidden" path="idPatient" name="idPatient"/><br/>

        <tr>
            <td>Date: </td>
            <td><sf:input name="date" path="date" type="datetime-local"/></td>
            <div class="error"><sf:errors path="patient"></sf:errors></div>
        </tr>
        <tr><td class="label">Length of Consultation: </td><td>
            <sf:input class="control" path="lengthOfConsultation" name="lengthOfConsultation"/><br/>
            <sf:errors path="lengthOfConsultation" cssClass="error"></sf:errors>
        </td></tr>
        <tr><td class="label">Notes: </td><td>
            <sf:input class="control" path="notes" name="notes"/><br/>
            <sf:errors path="notes" cssClass="error"></sf:errors>
        </td></tr>
        <tr>
            <td class="label">Doctor: </td>
            <td>
                <sf:select class="control" path="doctorName" name="doctorName" items="${allDoctors}"/><br/>
                <div class="error"><sf:errors path="doctorName"></sf:errors></div>
            </td>
        </tr>
        <tr><td class="label"> </td><td>
            <input class="control" value="Create consultation" type="submit"/>
        </td></tr>
    </table>
</sf:form>

</body>
</html>
