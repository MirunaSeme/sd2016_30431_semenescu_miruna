package com.mirunasemenescu.assign3.service;

import com.mirunasemenescu.assign3.dao.PatientDao;
import com.mirunasemenescu.assign3.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Miruna on 22-05-2016.
 */
@Service("patientService")
public class PatientService {

    private PatientDao patientDao;

    @Autowired
    public void setPatientDao(PatientDao patientDao) {
        this.patientDao = patientDao;
    }

    public void addPatient(Patient patient) {
        patientDao.createPatient(patient);
    }

    public boolean existsPatient(int id) {
        return patientDao.exists(id);
    }

    public List<Patient> getAllPatients() {
        return patientDao.getAllPatients();
    }

    public void deletePatient(int id) {
        patientDao.deletePatient(id);
    }

    public void updatePatient(Patient patient) {
        patientDao.updatePatient(patient);
    }

    public Patient getPatient(int id) {
        return patientDao.getPatient(id);
    }
}
