package com.mirunasemenescu.assign3.controller;

import com.mirunasemenescu.assign3.model.Consultation;
import com.mirunasemenescu.assign3.model.Patient;
import com.mirunasemenescu.assign3.model.User;
import com.mirunasemenescu.assign3.service.ConsultationService;
import com.mirunasemenescu.assign3.service.PatientService;
import com.mirunasemenescu.assign3.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Miruna on 22-05-2016.
 */
@Controller
public class ConsultationController {

    private ConsultationService consultationService;
    private PatientService patientService;
    private UserService userService;

    @Autowired
    public void setConsultationService(ConsultationService consultationService) {
        this.consultationService = consultationService;
    }

    @Autowired
    public void setPatientService(PatientService patientService){
        this.patientService = patientService;
    }

    @Autowired
    public void setUserService(UserService userService){
        this.userService = userService;
    }

    @RequestMapping(value="/addconsultation", method= RequestMethod.POST)
    public String doCreateCon(@Valid Consultation consultation, BindingResult result){
        if(result.hasErrors()){
            return "addconsultation";
        }
        String doctor = consultation.getDoctorName();
        User user = userService.getUser(doctor);
        consultation.setUser(user);
        int id = consultation.getIdPatient();
        Patient patient = patientService.getPatient(id);
        consultation.setPatient(patient);
        consultationService.addConsultation(consultation);
        return "consultationadded";
    }

    @ModelAttribute("allDoctors")
    public Map<String, String> populateDoctors() {
        Map<String, String> users = new HashMap<String, String>();
        List<User> userList = userService.getAllUsers();
        for (User value : userList) {
            if(value.getAuthority().equals("ROLE_DOCTOR")) {
                users.put(value.getUsername(), value.getUsername());
            }
        }
        return users;
    }

    @RequestMapping(value = "/addConsult", method = RequestMethod.GET)
    public String editUser(@RequestParam(value = "id", required = true) int id, Model model) {
        model.addAttribute("consultation", new Consultation());
        model.addAttribute("idPatient", id);
        return "addconsultation";
    }

    @RequestMapping("/consultations")
    public String showConsultations(Model model) {
        List<Consultation> consultations = consultationService.getAllConsultations();
        model.addAttribute("consultations", consultations);
        return "consultations";
    }

    @RequestMapping(value = "/editConsultation", method = RequestMethod.GET)
    public String editConsultation(@RequestParam(value = "id", required = true) int id, Model model) {
        Consultation consultation = consultationService.getConsultation(id);
        model.addAttribute("consultation", consultation);
        return "updateconsultation";
    }

    @RequestMapping(value = "/updateconsultation", method = RequestMethod.POST)
    public String editAccount(@Valid Consultation consultation, BindingResult result) {
        if (result.hasErrors())
            return "updateconsultation";
        String doctor = consultation.getDoctorName();
        User user = userService.getUser(doctor);
        consultation.setUser(user);
        int id = consultation.getIdPatient();
        Patient patient = patientService.getPatient(id);
        consultation.setPatient(patient);
        consultationService.updateConsultation(consultation);
        return "consultationupdated";
    }

    @RequestMapping(value = "/deleteConsultation", method = RequestMethod.GET)
    public String deleteUser(@RequestParam(value = "id", required = true) int id, Model model) {
        consultationService.deleteConsultation(id);
        return "consultationdeleted";
    }
}
