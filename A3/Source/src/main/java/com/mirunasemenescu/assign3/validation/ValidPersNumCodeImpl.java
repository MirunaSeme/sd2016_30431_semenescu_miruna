package com.mirunasemenescu.assign3.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Miruna on 22-05-2016.
 */
public class ValidPersNumCodeImpl implements ConstraintValidator<ValidPersNumCode, Long> {

    private int min;
    private long firstDigit;
    private String dateOfBirth;
    final static String DATE_FORMAT = "yyMMdd";


    public void initialize(ValidPersNumCode constraintAnnotation) {
        min = constraintAnnotation.min();
    }

    public boolean isValid(Long persNumCode, ConstraintValidatorContext context) {
        if(Long.toString(persNumCode).length() == min){
            firstDigit = Long.parseLong(Long.toString(persNumCode).substring(0, 1));
            System.out.println("First digit: " + firstDigit);
            if(firstDigit == 1 || firstDigit == 2) {
                dateOfBirth = Long.toString(persNumCode).substring(1, 7);
                System.out.println("Date of Birth: " + dateOfBirth);
                DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
                dateFormat.setLenient(false);
                try{
                    dateFormat.parse(dateOfBirth);
                    System.out.println("Date: " + dateFormat.toString());
                    return true;
                }catch(ParseException pe){
                    return false;
                }
            }
        }
        return false;
    }
}
