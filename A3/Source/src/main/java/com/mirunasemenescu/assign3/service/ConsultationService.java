package com.mirunasemenescu.assign3.service;

import com.mirunasemenescu.assign3.dao.ConsultationDao;
import com.mirunasemenescu.assign3.model.Consultation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Miruna on 22-05-2016.
 */
@Service("consultationService")
public class ConsultationService {

    private ConsultationDao consultationDao;

    @Autowired
    public void setConsultationDao(ConsultationDao consultationDao) {
        this.consultationDao = consultationDao;
    }

    public void addConsultation(Consultation consultation) {
        consultationDao.createConsultation(consultation);
    }

    public boolean existsConsultation(int id) {
        return consultationDao.exists(id);
    }

    public List<Consultation> getAllConsultations() {
        return consultationDao.getAllConsultations();
    }

    public void deleteConsultation(int id) {
        consultationDao.deleteConsultation(id);
    }

    public void updateConsultation(Consultation consultation) {
        consultationDao.updateConsultation(consultation);
    }

    public Consultation getConsultation(int id) {
        return consultationDao.getConsultation(id);
    }
}
