package com.mirunasemenescu.assign3.service;

import com.mirunasemenescu.assign3.dao.UserDao;
import com.mirunasemenescu.assign3.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Miruna on 22-05-2016.
 */
@Service("userService")
public class UserService {

    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void addUser(User user) {
        userDao.createUser(user);
    }

    public boolean existsUser(String username) {
        return userDao.exists(username);
    }

    @Secured("ROLE_ADMIN")
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    public void deleteUser(String username) {
        userDao.deleteUser(username);
    }

    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    public User getUser(String username) {
        return userDao.getUser(username);
    }

}
