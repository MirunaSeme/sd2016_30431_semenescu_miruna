package com.mirunasemenescu.assign3.dao;

import com.mirunasemenescu.assign3.model.Consultation;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Miruna on 22-05-2016.
 */
@Transactional
@Component("consultationDao")
public class ConsultationDao {

    @Autowired
    private SessionFactory sessionFactory;


    public Session session(){
        return sessionFactory.getCurrentSession();
    }

    public void createConsultation(Consultation consultation) {
        session().save(consultation);
    }

    public boolean exists(int id) {
        Criteria criteria = session().createCriteria(Consultation.class);
        // If we query for primary key, then we can simply use
        criteria.add(Restrictions.idEq(id));
        Consultation consultation = (Consultation) criteria.uniqueResult();
        return consultation != null;
    }

    public List<Consultation> getAllConsultations() {
        return session().createQuery("from com.mirunasemenescu.assign3.model.Consultation").list();
    }

    public Consultation getConsultation(int id) {
        Criteria crit = session().createCriteria(Consultation.class);
        // Here I expect a single object because we query on a single key NOT PK
        crit.add(Restrictions.eq("id", id));
        Consultation consultation=(Consultation) crit.uniqueResult();
        return consultation;
    }

    public void deleteConsultation(int id) {
        Consultation consultation = getConsultation(id);
        session().delete(consultation);
    }

    public void updateConsultation(Consultation consultation) {
        session().saveOrUpdate(consultation);
    }
}
