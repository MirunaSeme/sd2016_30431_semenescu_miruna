package com.mirunasemenescu.assign3.controller;

import com.mirunasemenescu.assign3.model.Patient;
import com.mirunasemenescu.assign3.service.PatientService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by Miruna on 22-05-2016.
 */
@Controller
public class PatientController {

    private static Logger logger = Logger.getLogger(HomeController.class);
    private PatientService patientService;
    private long persNumCode;

    @Autowired
    public void setPatientService(PatientService patientService) {
        this.patientService = patientService;
    }

    @RequestMapping(value="/docreate", method= RequestMethod.POST)
    public String doCreate(Model model, @Valid Patient patient, BindingResult result){
        if(result.hasErrors()){
            return "addpatient";
        }
        long persNumCode = patient.getPersNumCode();
        patient.setDateOfBirth(extractDateOfBirth(persNumCode));
        patientService.addPatient(patient);
        return "patientadded";
    }

    private String extractDateOfBirth(long persNumCode) {
        String dateOfBirth = "";
        String year = Long.toString(persNumCode).substring(1, 3);
        dateOfBirth = dateOfBirth.concat(year + ".");
        String month = Long.toString(persNumCode).substring(3, 5);
        dateOfBirth = dateOfBirth.concat(month + ".");
        String day = Long.toString(persNumCode).substring(5, 7);
        dateOfBirth = dateOfBirth.concat(day);
        return dateOfBirth;
    }

    @RequestMapping("/addpatient")
    public String addClient(Model model){
        model.addAttribute("patient", new Patient());
        return "addpatient";
    }

    @RequestMapping("/patients")
    public String showAdmin(Model model) {
        List<Patient> patients = patientService.getAllPatients();
        model.addAttribute("patients", patients);
        return "patients";
    }

    @RequestMapping(value = "/editPatient", method = RequestMethod.GET)
    public String editUser(@RequestParam(value = "id", required = true) int id, Model model) {
        Patient patient = patientService.getPatient(id);
        persNumCode = patient.getPersNumCode();
        model.addAttribute("patient", patient);
        return "updatepatient";
    }

    @RequestMapping(value = "/updatepatient", method = RequestMethod.POST)
    public String editAccount(@Valid Patient patient, BindingResult result, Principal principal) {
        if (result.hasErrors())
            return "updatepatient";
        if(persNumCode != patient.getPersNumCode()){
            patient.setDateOfBirth(extractDateOfBirth(patient.getPersNumCode()));
        }
        patientService.updatePatient(patient);
        return "patientupdated";
    }

}
