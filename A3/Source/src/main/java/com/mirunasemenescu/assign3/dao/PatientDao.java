package com.mirunasemenescu.assign3.dao;

import com.mirunasemenescu.assign3.model.Patient;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Miruna on 22-05-2016.
 */
@Transactional
@Component("patientDao")
    public class PatientDao {

    @Autowired
    private SessionFactory sessionFactory;

    public Session session(){
        return sessionFactory.getCurrentSession();
    }

    public void createPatient(Patient patient) {
            session().save(patient);
    }

    public List<Patient> getAllPatients() {
        return session().createQuery("from com.mirunasemenescu.assign3.model.Patient").list();
    }

    public boolean exists(int id) {
        Criteria criteria = session().createCriteria(Patient.class);
        // If we query for primary key, then we can simply use
        criteria.add(Restrictions.idEq(id));
        Patient patient = (Patient) criteria.uniqueResult();
        return patient != null;
    }

    public void deletePatient(int id) {
        Patient patient = getPatient(id);
        session().delete(patient);
    }

    public Patient getPatient(int id) {
        Criteria crit = session().createCriteria(Patient.class);
        // Here I expect a single object because we query on a single key NOT PK
        crit.add(Restrictions.eq("id", id));
        Patient patient=(Patient) crit.uniqueResult();
        return patient;
    }

    public void updatePatient(Patient patient) {
        session().saveOrUpdate(patient);
    }
}
