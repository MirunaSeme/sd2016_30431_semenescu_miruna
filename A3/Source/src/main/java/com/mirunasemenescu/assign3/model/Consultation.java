package com.mirunasemenescu.assign3.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Miruna on 22-05-2016.
 */
@Entity
@Table(name = "consultation")
public class Consultation {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    @Column(name = "date")
    private Date date;

    @ManyToOne
    @JoinColumn(name = "patient")
    private Patient patient;
    @ManyToOne
    @JoinColumn(name = "user")
    private User user;
    @Column(name = "lengthOfConsultation")
    private int lengthOfConsultation;
    @Column(name = "notes")
    private String notes;
    @Column(name = "doctorName")
    private String doctorName;

    private int idPatient;

    public Consultation(){
    }

    public Consultation(Date date, Patient patient, User user, int lengthOfConsultation, String notes, String doctorName){
        this.date = date;
        this.patient = patient;
        this.user = user;
        this.lengthOfConsultation = lengthOfConsultation;
        this.notes = notes;
        this.doctorName = doctorName;
    }

    public Consultation(int id, Date date, Patient patient, User user, int lengthOfConsultation, String notes, String doctorName){
        this.id = id;
        this.date = date;
        this.patient = patient;
        this.user = user;
        this.lengthOfConsultation = lengthOfConsultation;
        this.notes = notes;
        this.doctorName = doctorName;
    }

    public int getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(int idPatient) {
        this.idPatient = idPatient;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLengthOfConsultation() {
        return lengthOfConsultation;
    }

    public void setLengthOfConsultation(int lengthOfConsultation) {
        this.lengthOfConsultation = lengthOfConsultation;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
