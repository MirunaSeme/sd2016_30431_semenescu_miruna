package com.mirunasemenescu.assign3.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Miruna on 22-05-2016.
 */
@Controller
public class HomeController {

    @RequestMapping("/")
    public String showHome(Model model){
        return "home";
    }
}
