package com.mirunasemenescu.assign3.controller;

import com.mirunasemenescu.assign3.model.User;
import com.mirunasemenescu.assign3.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miruna on 22-05-2016.
 */
@Controller
public class LoginController {

    private static Logger logger = Logger.getLogger(HomeController.class);
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping("/login")
    public String showLogin() {
        return "login";
    }

    @RequestMapping("/logout")
    public String showLogout() {
        return "logout";
    }

    @RequestMapping("/denied")
    public String showDenied() {
        return "denied";
    }


    @RequestMapping(value = "/createuser", method = RequestMethod.POST)
    public String createAccount(@Valid User user, BindingResult result) {
        if (result.hasErrors()) {
            return "adduser";
        }
        user.setEnabled(true);
        if (userService.existsUser(user.getUsername())) {
            result.rejectValue("username", "DuplicateKey.user.username", "This username already exists!");
            return "adduser";
        }
        userService.addUser(user);
        return "useradded";
    }

    @RequestMapping("/adduser")
    public String showNewAccount(Model model) {
        model.addAttribute("user", new User());
        return "adduser";
    }

    @ModelAttribute("allAuthorities")
    public List<String> populateAuthorities(){
        List<String> authorities = new ArrayList<String>();
        authorities.add("ROLE_DOCTOR");
        authorities.add("ROLE_SECRETARY");
        return authorities;
    }

    @RequestMapping(value = "/editUser", method = RequestMethod.GET)
    public String editUser(@RequestParam(value = "id", required = true) String username, Model model) {
        User user = userService.getUser(username);
        model.addAttribute("user", user);
        return "updateuser";
    }

    @RequestMapping(value = "/updateuser", method = RequestMethod.POST)
    public String editAccount(@Valid User user, BindingResult result, Principal principal) {
        if (result.hasErrors())
            return "updateuser";
        if(user.getUsername().equals(principal.getName()) && (user.getAuthority().equals("ROLE_DOCTOR") || user.getAuthority().equals("ROLE_SECRETARY")) ){
            result.rejectValue("authority", "NonUpdateAdmin.user.authority");
            return "updateuser";
        }
        userService.updateUser(user);
        return "userupdated";
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public String deleteUser(@RequestParam(value = "id", required = true) String username, Model model, Principal principal) {
        if(username.equals(principal.getName())) {
            model.addAttribute("errorMsg", "Cannot delete yourself!");
            return "admin";
        }
        userService.deleteUser(username);
        return "userdeleted";
    }

    @RequestMapping("/admin")
    public String showAdmin(Model model) {
        List<User> users = userService.getAllUsers();
        model.addAttribute("users", users);
        return "admin";
    }
}
