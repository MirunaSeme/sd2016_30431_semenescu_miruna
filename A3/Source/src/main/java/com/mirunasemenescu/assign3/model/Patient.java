package com.mirunasemenescu.assign3.model;

import com.mirunasemenescu.assign3.validation.ValidIdenCard;
import com.mirunasemenescu.assign3.validation.ValidPersNumCode;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by Miruna on 22-05-2016.
 */
@Entity
@Table(name = "patient")
public class Patient {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "id")
    private int id;
    @Size(min = 5 ,max=60)
    @Pattern(regexp = ".* .*")
    @Column(name = "name")
    private String name;
    @NotNull
    @ValidIdenCard(min = 6)
    @Column(name = "idenCard")
    private int idenCard;
    @ValidPersNumCode(min = 13)
    @Column(name = "persNumCode")
    private long persNumCode;
    @NotEmpty
    @Column(name = "dateOfBirth")
    private String dateOfBirth = "hjhjg";
    @NotEmpty
    @Column(name = "address")
    private String address;

    public Patient(){}

    public Patient(String name, int idenCard, long persNumCode, String dateOfBirth, String address){
        this.name = name;
        this.idenCard = idenCard;
        this.persNumCode = persNumCode;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
    }

    public Patient(int id, String name, int idenCard, long persNumCode, String dateOfBirth, String address){
        this.id = id;
        this.name = name;
        this.idenCard = idenCard;
        this.persNumCode = persNumCode;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdenCard() {
        return idenCard;
    }

    public void setIdenCard(int idenCard) {
        this.idenCard = idenCard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPersNumCode() {
        return persNumCode;
    }

    public void setPersNumCode(long persNumCode) {
        this.persNumCode = persNumCode;
    }
}
