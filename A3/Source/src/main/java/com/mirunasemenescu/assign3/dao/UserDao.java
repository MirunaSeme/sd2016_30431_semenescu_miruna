package com.mirunasemenescu.assign3.dao;

import com.mirunasemenescu.assign3.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

/**
 * Created by Miruna on 22-05-2016.
 */
@Transactional
@Component("userDao")
public class UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    public Session session(){
        return sessionFactory.getCurrentSession();
    }

    public List<User> getAllUsers(){
        return session().createQuery("from com.mirunasemenescu.assign3.model.User").list();
    }

    public void createUser(User user) {
        session().save(user);
    }

    public boolean exists(String username){
        Criteria criteria = session().createCriteria(User.class);
        // If we query for primary key, then we can simply use
        criteria.add(Restrictions.idEq(username));
        User user = (User)criteria.uniqueResult();
        return user != null;
    }

    public void deleteUser(String username){
        User user = getUser(username);
        session().delete(user);
    }

    public void updateUser(User user) {
        session().saveOrUpdate(user);
    }

    public User getUser(String username) {
        Criteria crit = session().createCriteria(User.class);
        crit.add(Restrictions.eq("username", username)); //Metoda asta e ok daca you query for columns which are not PK
        User user=(User) crit.uniqueResult();
        return user;
    }
}
