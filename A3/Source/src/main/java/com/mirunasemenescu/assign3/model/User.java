package com.mirunasemenescu.assign3.model;

import com.mirunasemenescu.assign3.validation.ValidPersNumCode;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by Miruna on 22-05-2016.
 */
@Entity
@Table(name = "user")
    public class User {

        @Id
        @NotBlank
        @Size(min = 4, max = 15)
        @Pattern(regexp = "^\\w{4,}$")
        @Column(name = "username")
        private String username;
        @NotBlank
        @Pattern(regexp = "^\\S+$")
        @Size(min = 4, max = 15)
        @Column(name = "password")
        private String password;
        @ValidPersNumCode(min = 13)
        @Column(name = "persNumCode")
        private long persNumCode;
        @Column(name = "enabled")
        private boolean enabled = false;
        @Column(name = "authority")
        private String authority;

    public User() {
    }

    public User(String username, String password, long persNumCode, boolean enabled, String authority, String type) {
        this.authority = authority;
        this.enabled = enabled;
        this.password = password;
        this.username = username;
        this.persNumCode = persNumCode;

    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getPersNumCode() {
        return persNumCode;
    }

    public void setPersNumCode(long persNumCode) {
        this.persNumCode = persNumCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (getPersNumCode() != user.getPersNumCode()) return false;
        if (isEnabled() != user.isEnabled()) return false;
        if (getUsername() != null ? !getUsername().equals(user.getUsername()) : user.getUsername() != null)
            return false;
        if (getPassword() != null ? !getPassword().equals(user.getPassword()) : user.getPassword() != null)
            return false;
        return getAuthority() != null ? getAuthority().equals(user.getAuthority()) : user.getAuthority() == null;

    }

    @Override
    public int hashCode() {
        int result = getUsername() != null ? getUsername().hashCode() : 0;
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (int) (getPersNumCode() ^ (getPersNumCode() >>> 32));
        result = 31 * result + (isEnabled() ? 1 : 0);
        result = 31 * result + (getAuthority() != null ? getAuthority().hashCode() : 0);
        return result;
    }
}
