package com.assignment2.service;

import com.assignment2.dao.LibraryDao;
import com.assignment2.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Miruna on 15-05-2016.
 */
@Component("libraryService")
public class LibraryService {

    private LibraryDao libraryDao;

    @Autowired
    public void setLibraryDao(LibraryDao libraryDao) {
        this.libraryDao = libraryDao;
    }

    public void marshallLibrary(Book book){
        libraryDao.marshallLibrary(book);
    }

    public List<Book> unmarshallLibrary() {
        return libraryDao.unmarshallLibrary();
    }

    public void deleteBook(Integer id) {
        libraryDao.deleteBook(id);
    }

    public Book getCurrentBook(Integer id) {
        return libraryDao.getCurrentBook(id);
    }

    public void updateBook(Book book) {
        libraryDao.updateBook(book);
    }

    public void sellBook(Integer id) {
        libraryDao.sellBook(id);
    }
}
