package com.assignment2.service;

import com.assignment2.dao.SearchDao;
import com.assignment2.model.Book;
import com.assignment2.model.Search;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

/**
 * Created by Miruna on 17-05-2016.
 */
@Component("searchService")
public class SearchService {

    private SearchDao searchDao;

    @Autowired
    public void setSearchDao(SearchDao searchDao) {
        this.searchDao = searchDao;
    }

    public void search(Search search) {
        searchDao.search(search);
    }

    public List<Book> unmarshallSearchLibrary(File file) {
        return searchDao.unmarshallLibrary(file);
    }

    public void sellBook(Integer id) {
        searchDao.sellBook(id);
    }

    public int getHashMap(int id) {
        return searchDao.getHashmap().get(id);
    }
}
