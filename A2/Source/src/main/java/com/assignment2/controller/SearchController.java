package com.assignment2.controller;

import com.assignment2.model.Book;
import com.assignment2.model.Search;
import com.assignment2.service.LibraryService;
import com.assignment2.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.io.File;
import java.util.List;

/**
 * Created by Miruna on 17-05-2016.
 */
@Controller
public class SearchController {

    private SearchService searchService;
    private LibraryService libraryService;

    @Autowired
    public void setLibraryService(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    @Autowired
    public void setSearchService(SearchService searchService) {
        this.searchService = searchService;
    }

    @RequestMapping(value = "/dosearchbook", method = RequestMethod.POST)
    public String doCreate(Model model, @Valid Search search, BindingResult result) {
        if (result.hasErrors()) {
            return "searchbook";
        }
        searchService.search(search);
        List<Book> library = searchService.unmarshallSearchLibrary(new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\libraryToSearch.xml"));
        model.addAttribute("library",library);
        return "libraryForSearch";
    }

    @RequestMapping("/searchbook")
    public String addBook(Model model) {
        model.addAttribute("search", new Search());
        return "searchbook";
    }

    @RequestMapping("/libraryForSearch")
    public String showSearchLibrary(Model model){
        List<Book> library = searchService.unmarshallSearchLibrary(new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\libraryToSearch.xml"));
        model.addAttribute("library",library);
        return "libraryForSearch";
    }

    @RequestMapping(value = "/sellBook", method = RequestMethod.GET)
    public String sellBook(@RequestParam(value = "id", required = true) Integer id, Model model) {
        List<Book> library = searchService.unmarshallSearchLibrary(new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\libraryToSearch.xml"));
        if (library.get(id).getQuantity() == 0){
            model.addAttribute("errorMsg", "Cannot sell, out of stock!");
            List<Book> libraryList = searchService.unmarshallSearchLibrary(new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\libraryToSearch.xml"));
            model.addAttribute("library",libraryList);
            return "libraryForSearch";
        }
        searchService.sellBook(id);
        int idLibrary = searchService.getHashMap(id);
        libraryService.sellBook(idLibrary);
        List<Book> bla = searchService.unmarshallSearchLibrary(new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\libraryToSearch.xml"));
        model.addAttribute("library",bla);
        return "libraryForSearch";
    }
}
