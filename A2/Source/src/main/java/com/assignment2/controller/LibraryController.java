package com.assignment2.controller;
import com.assignment2.model.Book;
import com.assignment2.service.CreatePDF;
import com.assignment2.service.LibraryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.InputStreamEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Miruna on 15-05-2016.
 */
@Controller
public class LibraryController {

    private LibraryService libraryService;
    private CreatePDF createPDF;

    @Autowired
    public void setLibraryService(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    @Autowired
    public void setCreatePDF(CreatePDF createPDF) {
        this.createPDF = createPDF;
    }

    @RequestMapping("/library")
    public String showLibrary(Model model){
        List<Book> library = libraryService.unmarshallLibrary();
        model.addAttribute("library",library);
        return "library";
    }

    @RequestMapping(value = "/doaddbook", method = RequestMethod.POST)
    public String doCreate(Model model, @Valid Book book, BindingResult result) {
        if (result.hasErrors()) {
            return "addbook";
        }
        List<Book> libraryList = libraryService.unmarshallLibrary();
        int index = libraryList.get(libraryList.size()-1).getId();
        index++;
        book.setId(index);
        libraryService.marshallLibrary(book);
        return "addedbook";
    }

    @RequestMapping("/addbook")
    public String addBook(Model model) {
        model.addAttribute("book", new Book());
        return "addbook";
    }

    @ModelAttribute("allGenre")
    public List<String> populateGenres() {
        ArrayList<String> genres = new ArrayList<String>();
        genres.add("Fiction");
        genres.add("Non-Fiction");
        genres.add("Fantasy");
        genres.add("Science Fiction");
        genres.add("Horror");
        genres.add("Romantic");
        genres.add("Thriller");
        genres.add("Comedy");
        genres.add("Mystery");
        genres.add("History");
        genres.add("Drama");
        return genres;
    }

    @RequestMapping(value = "/deleteBook", method = RequestMethod.GET)
    public String deleteBook(@RequestParam(value = "id", required = true) Integer id, Model model) {
        libraryService.deleteBook(id);
        return "deletedbook";
    }

    @RequestMapping(value = "/updatebook", method = RequestMethod.POST)
    public String updateBook2(@Valid Book book, BindingResult result) {
        if (result.hasErrors())
            return "updatebook";
        libraryService.updateBook(book);
        return "updatedbook";
    }

    @RequestMapping(value = "/updateBook", method = RequestMethod.GET)
    public String updateBook(@RequestParam(value = "id", required = true) Integer id, Model model) {
        Book book = libraryService.getCurrentBook(id);
        model.addAttribute("book", book);
        return "updatebook";
    }

    @RequestMapping(value = "/downloadPDF")
    public void downloadPDF(HttpServletRequest request, HttpServletResponse response) throws IOException {

        final ServletContext servletContext = request.getSession().getServletContext();
        final File tempDirectory = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        final String temperotyFilePath = tempDirectory.getAbsolutePath();

        String fileName = "OutOfStock.pdf";
        response.setContentType("application/pdf");
        response.setHeader("Content-disposition", "attachment; filename="+ fileName);

        try {
            createPDF.createPDF(temperotyFilePath+"\\"+fileName);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            baos = convertPDFToByteArrayOutputStream(temperotyFilePath+"\\"+fileName);
            OutputStream os = response.getOutputStream();
            baos.writeTo(os);
            os.flush();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private ByteArrayOutputStream convertPDFToByteArrayOutputStream(String fileName) {

        InputStream inputStream = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {

            inputStream = new FileInputStream(fileName);
            byte[] buffer = new byte[1024];
            baos = new ByteArrayOutputStream();

            int bytesRead;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                baos.write(buffer, 0, bytesRead);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return baos;
    }

    @RequestMapping(value = "/downloadCSV")
    public void downloadCSV(HttpServletResponse response) throws IOException {

        response.setContentType("text/csv");
        String reportName = "CSV_Report_Name.csv";
        response.setHeader("Content-disposition", "attachment;filename="+reportName);

        ArrayList<String> rows = new ArrayList<String>();
        rows.add("Title,Author,Genre,Price");
        rows.add("\n");

        List<Book> library = libraryService.unmarshallLibrary();
        List<Book> outOfStock = new ArrayList<Book>();
        for(Book value: library){
            if(value.getQuantity() == 0){
                outOfStock.add(value);
            }
        }
        for (int i = 0; i < outOfStock.size(); i++) {
            rows.add(outOfStock.get(i).getTitle()+","+outOfStock.get(i).getAuthor()+","+outOfStock.get(i).getGenre()+","+
                    Double.toString(outOfStock.get(i).getPrice()));
            rows.add("\n");
        }

        Iterator<String> iter = rows.iterator();
        while (iter.hasNext()) {
            String outputString = (String) iter.next();
            response.getOutputStream().print(outputString);
        }
        response.getOutputStream().flush();
    }

}

