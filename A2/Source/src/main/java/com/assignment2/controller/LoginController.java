package com.assignment2.controller;

import com.assignment2.dao.UserDao;
import com.assignment2.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miruna on 19-05-2016.
 */
@Controller
public class LoginController {

    private UserDao userDao;
    private String previousUsername;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @RequestMapping("/login")
    public String showLogin() {
        return "login";
    }

    @RequestMapping("/logout")
    public String showLogout() {
        return "logout";
    }

    @RequestMapping(value = "/createaccount", method = RequestMethod.POST)
    public String createAccount(@Valid User user, BindingResult result) {
        if (result.hasErrors()) {
            return "adduser";
        }
        user.setEnabled(true);
        if (userDao.exists(user.getUsername())) {
            result.rejectValue("username", "DuplicateKey.user.username", "This username already exists!");
            return "adduser";
        }
        userDao.marshallUser(user);
        return "addeduser";
    }

    @RequestMapping("adduser")
    public String showNewUser(Model model) {
        model.addAttribute("user", new User());
        return "adduser";
    }

    @ModelAttribute("allAuthorities")
    public List<String> populateAuthorities(Principal principal){
        List<String> authorities = new ArrayList<String>();
        authorities.add("ROLE_USER");
        return authorities;
    }

    @RequestMapping("/delete")
    public String showDelete(Model model) {
        List<String> users = new ArrayList<String>();
        List<User> usersList = userDao.unmarshallUser();
        for(User value: usersList) {
            users.add(value.getUsername());
        }
        model.addAttribute("bla", users);
        return "delete";
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
    public String deleteUser(@RequestParam(value = "id", required = true) String username, Model model, Principal principal) {
        if(username.equals(principal.getName())) {
            model.addAttribute("errorMsg", "Cannot delete yourself!");
            return "admin";
        }
        userDao.deleteUser(username);
        return "userdeleted";
    }

    @RequestMapping(value = "/editUser", method = RequestMethod.GET)
    public String editUser(@RequestParam(value = "id", required = true) String username, Model model) {
        User user = userDao.getUserbyUsername(username);
        model.addAttribute("user", user);
        this.previousUsername = user.getUsername();
        return "updateuser";
    }

    @RequestMapping(value = "/updateuser", method = RequestMethod.POST)
    public String editAccount(@Valid User user, BindingResult result, Principal principal) {
        if (result.hasErrors())
            return "updateuser";
        if (user.getUsername().equals(principal.getName()) && user.getAuthority().equals("ROLE_USER")) {
            result.rejectValue("authority", "NonUpdateAdmin.user.authority");
            return "updateuser";
        }
        userDao.updateUser(user, previousUsername);
        return "updateduser";
    }

    @RequestMapping("/admin")
    public String showAdmin(Model model) {
        List<User> users = userDao.unmarshallUser();
        model.addAttribute("users", users);
        return "admin";
    }
}
