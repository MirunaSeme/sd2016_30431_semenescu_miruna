package com.assignment2.model;

import com.sun.xml.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miruna on 15-05-2016.
 */
@XmlRootElement(name = "library")
@XmlAccessorType(XmlAccessType.FIELD)
public class Library {

    private List<Book> library = null;

    @XmlElement(value = "book")
    public List<Book> getLibrary() {
        return library;
    }

    @XmlElement(value = "book")
    public void setLibrary(List<Book> library) {
        this.library = library;
    }
}
