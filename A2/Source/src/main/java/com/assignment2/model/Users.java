package com.assignment2.model;

import com.sun.xml.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Miruna on 18-05-2016.
 */
@XmlRootElement(name = "users")
@XmlAccessorType(XmlAccessType.FIELD)
public class Users {

    private List<User> userList;

    public Users(){}

    public Users(List<User> userList){this.userList = userList;}

    @XmlElement(value = "user")
    public List<User> getUserList() {
        return userList;
    }

    @XmlElement(value = "user")
    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users users = (Users) o;
        return getUserList() != null ? getUserList().equals(users.getUserList()) : users.getUserList() == null;

    }

    @Override
    public int hashCode() {
        return getUserList() != null ? getUserList().hashCode() : 0;
    }
}
