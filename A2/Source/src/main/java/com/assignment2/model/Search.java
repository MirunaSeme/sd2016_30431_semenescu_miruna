package com.assignment2.model;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by Miruna on 17-05-2016.
 */
public class Search {

    @NotEmpty
    private String search;

    public Search(){}

    public Search(String search){
        this.search = search;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
