package com.assignment2.model;

import com.assignment2.validator.ValidPersNumCode;
import org.hibernate.annotations.Entity;
import org.hibernate.annotations.Table;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Id;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Miruna on 18-05-2016.
 */
@Entity
@XmlRootElement(name = "user")
@XmlAccessorType(XmlAccessType.FIELD)
public class User implements Serializable, UserDetails{

    private static final long serialVersionUID = -3502744983433694888L;

    @Id
    @NotBlank
    @Size(min = 4, max = 15)
    @Pattern(regexp = "^\\w{4,}$")
    private String username;
    @NotBlank
    @Pattern(regexp = "^\\S+$")
    @Size(min = 4, max = 15)
    private String password;
    @ValidPersNumCode(min = 13)
    private long persNumCode;
    private boolean enabled = false;
    private String authority;

    public User() {}

    public User(String username, String password, long persNumCode, boolean enabled, String authority) {
        this.authority = authority;
        this.enabled = enabled;
        this.password = password;
        this.username = username;
        this.persNumCode = persNumCode;
    }

    public String getAuthority() {
        return this.authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
        list.add(new GrantedAuthority() {
            public String getAuthority() {
                return authority;
            }
        });
        return list;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public long getPersNumCode() {
        return persNumCode;
    }

    public void setPersNumCode(long persNumCode) {
        this.persNumCode = persNumCode;
    }

    public String getUsername() {
        return username;
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (getPersNumCode() != user.getPersNumCode()) return false;
        if (isEnabled() != user.isEnabled()) return false;
        if (!getUsername().equals(user.getUsername())) return false;
        if (!getPassword().equals(user.getPassword())) return false;
        return getAuthority().equals(user.getAuthority());

    }

    @Override
    public int hashCode() {
        int result = getUsername().hashCode();
        result = 31 * result + getPassword().hashCode();
        result = 31 * result + (int) (getPersNumCode() ^ (getPersNumCode() >>> 32));
        result = 31 * result + (isEnabled() ? 1 : 0);
        result = 31 * result + getAuthority().hashCode();
        return result;
    }
}
