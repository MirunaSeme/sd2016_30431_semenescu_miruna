package com.assignment2.dao;

import com.assignment2.model.Library;
import com.assignment2.model.User;
import com.assignment2.model.Users;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Miruna on 18-05-2016.
 */
@Component("userDao")
public class UserDao {

    public User getUserbyUsername(String s) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Users.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            Users users = (Users) jaxbUnmarshaller.unmarshal( new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\users.xml") );
            List<User> userList = users.getUserList();
            for(User value: userList){
                if(value.getUsername().equals(s)){
                    return value;
                }
            }
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void marshallUser(User user){
        List<User> userList = unmarshallUser();
        userList.add(user);
        Users users = new Users();
        users.setUserList(userList);
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Users.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(users, System.out);
            jaxbMarshaller.marshal(users, new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\users.xml"));
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
    }

    public List<User> unmarshallUser() {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Users.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Users users = (Users) jaxbUnmarshaller.unmarshal( new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\users.xml") );
            return users.getUserList();
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    public boolean exists(String username) {
        List<User> userList = unmarshallUser();
        if(userList == null){
            return false;
        }
        for(User value: userList){
            if(value.getUsername().equals(username)){
                return true;
            }
        }
        return false;
    }

    public void deleteUser(String username) {
        List<User> userList = unmarshallUser();
        for(User value: userList){
            if(value.getUsername().equals(username)){
                userList.remove(value);
            }
        }
        Users users = new Users();
        users.setUserList(userList);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Users.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(users, System.out);
            jaxbMarshaller.marshal(users, new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\users.xml"));
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
    }

    public void updateUser(User user, String previousUsername) {
        List<User> userList = unmarshallUser();
        for(User value: userList){
            if(value.getUsername().equals(previousUsername)){
                value.setUsername(user.getUsername());
                value.setPassword(user.getPassword());
                value.setPersNumCode(user.getPersNumCode());
                value.setAuthority(user.getAuthority());
            }
        }

        Users users = new Users();
        users.setUserList(userList);
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Users.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(users, System.out);
            jaxbMarshaller.marshal(users, new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\users.xml"));
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
    }
}
