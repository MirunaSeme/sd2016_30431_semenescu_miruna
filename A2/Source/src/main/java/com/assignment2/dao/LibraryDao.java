package com.assignment2.dao;

import com.assignment2.model.Book;
import com.assignment2.model.Library;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miruna on 15-05-2016.
 */
@Component("libraryDao")
public class LibraryDao {

    private static Logger logger = Logger.getLogger(LibraryDao.class);

    public void marshallLibrary(Book book){
        List<Book> libraryList = unmarshallLibrary();
        libraryList.add(book);
        Library library = new Library();
        library.setLibrary(libraryList);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(library, System.out);
            jaxbMarshaller.marshal(library, new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\library.xml"));
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
    }

    public List<Book> unmarshallLibrary(){
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            Library lib = (Library) jaxbUnmarshaller.unmarshal( new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\library.xml") );

            return lib.getLibrary();
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    public Library unmarshall(){
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            Library lib = (Library) jaxbUnmarshaller.unmarshal( new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\library.xml") );

            return lib;
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
        return null;
    }


    public void deleteBook(int id) {
        List<Book> libraryList = unmarshallLibrary();
        libraryList.remove(id);
        for(int i = id; i < libraryList.size(); i++){
            Book book = libraryList.get(i);
            book.setId(i);
            libraryList.set(i,book);
        }
        Library library = new Library();
        library.setLibrary(libraryList);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(library, System.out);
            jaxbMarshaller.marshal(library, new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\library.xml"));
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }

    }

    public Book getCurrentBook(Integer id) {
        List<Book> libraryList = unmarshallLibrary();
        return libraryList.get(id);
    }

    public void updateBook(Book book) {
        int id = book.getId();
        Library library = unmarshall();
        List<Book> libraryList = new ArrayList<Book>();
        libraryList = library.getLibrary();
        libraryList.set(id, book);
        library.setLibrary(libraryList);
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(library, System.out);
            jaxbMarshaller.marshal(library, new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\library.xml"));
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
    }

    public List<Book> unmarshallLibrary(File file){
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            Library lib = (Library) jaxbUnmarshaller.unmarshal(file);

            return lib.getLibrary();
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
        return null;
    }


    public void sellBook(Integer id) {
        List<Book> libraryList = unmarshallLibrary(new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\library.xml"));
        Book book = libraryList.get(id);
        int newQuantity = book.getQuantity();
        newQuantity--;
        book.setQuantity(newQuantity);
        libraryList.set(id,book);
        Library library = new Library();
        library.setLibrary(libraryList);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(library, System.out);
            jaxbMarshaller.marshal(library, new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\library.xml"));
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
    }
}
