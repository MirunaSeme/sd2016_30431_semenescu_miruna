package com.assignment2.dao;

import com.assignment2.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created by Miruna on 18-05-2016.
 */
@Component
public class UserDetailsServiceDao implements UserDetailsService{

    @Autowired
    private UserDao userDao;

    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userDao.getUserbyUsername(s);
        if(user == null){
            throw new UsernameNotFoundException("Username not found");
        } else {
            return user;
        }
    }

    public void setUserDao(UserDao userDao){
        this.userDao = userDao;
    }

    public UserDao getUserDao(){
        return userDao;
    }

}
