package com.assignment2.dao;

import com.assignment2.model.Book;
import com.assignment2.model.Library;
import com.assignment2.model.Search;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Miruna on 17-05-2016.
 */
@Component("searchDao")
public class SearchDao {

    private HashMap<Integer, Integer> hashmap = new HashMap<Integer, Integer>();

    public HashMap<Integer, Integer> getHashmap() {
        return hashmap;
    }

    public void setHashmap(HashMap<Integer, Integer> hashmap) {
        this.hashmap = hashmap;
    }

    public List<Book> unmarshallLibrary(File file){
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

            Library lib = (Library) jaxbUnmarshaller.unmarshal(file);

            return lib.getLibrary();
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    public void search(Search search) {
        // Empty the search file for the library before performing a new search
        File searchFile = new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\libraryToSearch.xml");
        try {
            PrintWriter writer = new PrintWriter(searchFile);
            writer.print("");
            writer.close();
        }catch (FileNotFoundException e){
            e.getCause();
        }
        String searchWord = search.getSearch().toLowerCase();
        List<Book> libraryList = unmarshallLibrary(new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\library.xml"));
        int index = 0;
        for(Book value : libraryList){
            String title = value.getTitle().toLowerCase();
            String author = value.getAuthor().toLowerCase();
            String genre = value.getGenre().toLowerCase();
            int id = value.getId();
            if(title.contains(searchWord) || author.contains(searchWord) || genre.contains(searchWord)){
                // Put library id to library for search id
                hashmap.put(index, id);
                value.setId(index);
                marshallLibrary(value);
                index++;
            }
        }
    }

    public void marshallLibrary(Book book){
        List<Book> libraryList = new ArrayList<Book>();
        if(book.getId() != 0 ) {
            libraryList = unmarshallLibrary(new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\libraryToSearch.xml"));
        }
        libraryList.add(book);
        Library library = new Library();
        library.setLibrary(libraryList);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(library, System.out);
            jaxbMarshaller.marshal(library, new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\libraryToSearch.xml"));
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
    }

    public void sellBook(Integer id) {
        List<Book> libraryList = unmarshallLibrary(new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\libraryToSearch.xml"));
        Book book = libraryList.get(id);
        int newQuantity = book.getQuantity();
        newQuantity--;
        book.setQuantity(newQuantity);
        libraryList.set(id,book);
        Library library = new Library();
        library.setLibrary(libraryList);

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Library.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(library, System.out);
            jaxbMarshaller.marshal(library, new File("F:\\Java\\EclipseWorkspace\\SDAssignment2\\libraryToSearch.xml"));
        }catch(JAXBException e){
            System.out.println(e.getMessage());
        }
    }
}
