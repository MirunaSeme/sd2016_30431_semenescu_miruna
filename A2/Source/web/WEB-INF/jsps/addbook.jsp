<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 16-05-2016
  Time: 10:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Add a book to the online library</title>
</head>
<body>

<sf:form method="post" action="${pageContext.request.contextPath}/doaddbook" commandName="book">
    <table class="formtable">
        <sf:input type="hidden" path="id" name="id"/><br/>
        <tr>
            <td class="label">Book Title:</td>
            <td>
                <sf:input class="control" path="title" name="title"/><br/>
                <sf:errors path="title" cssClass="error"></sf:errors>
            </td>
        </tr>
        <tr>
            <td class="label">Author:</td>
            <td>
                <sf:input class="control" path="author" name="author"/><br/>
                <sf:errors path="author" cssClass="error"></sf:errors>
            </td>
        </tr>
        <tr>
            <td class="label">Genre:</td>
            <td>
                <sf:select class="control" path="genre" name="genre" items="${allGenre}"/><br/>
                <sf:errors path="genre" cssClass="error"></sf:errors>
            </td>
        </tr>
        <tr>
            <td class="label">Price:</td>
            <td>
                <sf:input class="control" path="price" name="price"/><br/>
                <sf:errors path="price" cssClass="error"></sf:errors>
            </td>
        </tr>
        <tr>
            <td class="label">Quantity:</td>
            <td>
                <sf:input class="control" path="quantity" name="quantity"/><br/>
                <sf:errors path="quantity" cssClass="error"></sf:errors>
            </td>
        </tr>
        <tr>
            <td class="label"></td>
            <td>
                <input class="control" value="Add book" type="submit"/>
            </td>
        </tr>
    </table>
</sf:form>

</body>
</html>
