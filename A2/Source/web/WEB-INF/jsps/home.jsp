<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 15-05-2016
  Time: 10:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Title</title>
</head>
<body>

<div id="header">
    <h1>Welcome!</h1>
</div>

<div id="left">

<p><a href="${pageContext.request.contextPath}/searchbook" target="target">Search Book</a></p>
<p><a href="${pageContext.request.contextPath}/adduser" target="target">Add User</a></p>
<sec:authorize access="!isAuthenticated()">
    <p><a target="target" href="<c:url value='/login'/>">Log in</a></p>
</sec:authorize>
<sec:authorize access="isAuthenticated()">
    <p><a target="target" href="<c:url value='/j_spring_security_logout'/>">Log out</a></p>
</sec:authorize>
<sec:authorize access="hasRole('ROLE_ADMIN')">
    <p><a href="${pageContext.request.contextPath}/library" target="target">Library</a></p>
    <p><a target="target" href="<c:url value='/admin'/>">Users</a></p>
    <p><a href="${pageContext.request.contextPath}/addbook" target="target">Add Book</a></p>
    <form:form action="downloadPDF" method="post" id="downloadPDF">
        <input id="submitId" type="submit" value="Download PDF">
    </form:form>
    <form:form action="downloadCSV" method="post" id="downloadCSV">
        <input id="submitId" type="submit" value="Download CSV">
    </form:form>
</sec:authorize>

</div>

<div class="target">
    <iframe name="target" height="500px" width="1000px"></iframe>
</div>

</body>
</html>
