<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 19-05-2016
  Time: 1:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="${pageContext.request.contextPath}/static/script/jquery.js"></script>

    <script type="text/javascript">

        function onLoad() {
            $("#password").keyup(checkPasswordsMatch);
            $("#confirmpassword").keyup(checkPasswordsMatch);
            $("#details").submit(canSubmit);
        }

        function canSubmit() {
            var password = $("#password").val();
            var confirmpassword = $("#confirmpassword").val();

            if (password != confirmpassword) {
                alert("<fmt:message key='UnmatchedPasswords.user.password'/>");
                return false;
            } else {
                return true;
            }
        }

        function checkPasswordsMatch() {
            var password = $("#password").val();
            var confirmpassword = $("#confirmpassword").val();

            if (password.length > 3 || confirmpassword.length > 3) {
                if (password == confirmpassword) {
                    $("#matchpass").text("<fmt:message key='MatchedPasswords.user.password'/>");
                    $("#matchpass").addClass("valid");
                    $("#matchpass").removeClass("error");
                } else {
                    $("#matchpass").text("<fmt:message key='UnmatchedPasswords.user.password'/>");
                    $("#matchpass").addClass("error");
                    $("#matchpass").removeClass("valid");
                }
            }
        }

        $(document).ready(onLoad);

    </script>

    <title>Create New Account</title>
</head>
<body>

<h2>Edit Current Account</h2>

<sf:form id="details" method="post" action="${pageContext.request.contextPath}/updateuser" commandName="user">
    <table class="formtable">

        <sf:input type="hidden" path="enabled" name="enabled"/><br/>

        <tr>
            <td class="label">Username:</td>
            <td>
                <sf:input class="control" path="username" name="username"/><br/>
                <div class="error"><sf:errors path="username"></sf:errors></div>
            </td>
        </tr>
        <tr>
            <td class="label">Personal numerical code:</td>
            <td>
                <sf:input class="control" path="persNumCode" name="persNumCode"/><br/>
                <div class="error"><sf:errors path="persNumCode"></sf:errors></div>
            </td>
        </tr>
        <tr>
            <td class="label">Authority:</td>
            <td>
                <sf:select class="control" path="authority" name="authority" items="${allAuthorities}"/><br/>
                <div class="error"><sf:errors path="authority"></sf:errors></div>
            </td>
        </tr>
        <tr>
            <td class="label">Password:</td>
            <td>
                <sf:input id="password" class="control" path="password" name="password" type="password"/><br/>
                <div class="error"><sf:errors path="password"></sf:errors></div>
            </td>
        </tr>
        <tr>
            <td class="label">Confirm Password:</td>
            <td>
                <input id="confirmpassword" class="control" name="confirmpassword" type="password"/>
                <div id="matchpass" class="valid"></div>
            </td>
        </tr>
        <tr>
            <td class="label"></td>
            <td>
                <input class="control" value="Update user" type="submit"/>
            </td>
        </tr>
    </table>
</sf:form>

</body>
</html>
