<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 15-05-2016
  Time: 4:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Libraries</title>
</head>
<body>

<h2>Library</h2>

<table>
    <tr><td>Title</td><td>Author</td><td>Genre</td><td>Price</td><td>Stock</td><td>Delete Book</td><td>Update Book</td></tr>
    <c:forEach var="book" items="${library}">
        <tr>
            <td><c:out value="${book.title}"/></td>
            <td><c:out value="${book.author}"/></td>
            <td><c:out value="${book.genre}"/></td>
            <td><c:out value="${book.price}"/></td>
            <td><c:out value="${book.quantity}"/></td>
            <td><a href=<c:url value="/deleteBook?id=${book.id}"/>>Delete</a></td>
            <td><a href=<c:url value="/updateBook?id=${book.id}"/>>Update</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>

