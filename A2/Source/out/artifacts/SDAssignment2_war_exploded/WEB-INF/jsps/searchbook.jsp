<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Miruna
  Date: 17-05-2016
  Time: 10:18 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/static/css/main.css" rel="stylesheet" type="text/css"/>
    <title>Search for a desired book</title>
</head>
<body>

<sf:form method="post" action="${pageContext.request.contextPath}/dosearchbook" commandName="search">
    <table class="formtable">
        <tr>
            <td class="label">Search:</td>
            <td>
                <sf:input class="control" path="search" name="search"/><br/>
                <sf:errors path="search" cssClass="error"></sf:errors>
            </td>
        </tr>
        <tr>
            <td class="label"></td>
            <td>
                <input class="control" value="Search book" type="submit"/>
            </td>
        </tr>
    </table>
</sf:form>

</body>
</html>
